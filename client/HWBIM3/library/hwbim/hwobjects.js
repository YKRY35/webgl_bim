function HWObjects() {
    this.bimObj = new HWObjects.BIMObj();
    this.inspObj = new HWObjects.InspObj();
}

HWObjects.FireObj = function () {

}

HWObjects.InspObj = function () {
    this.points = [];
}

HWObjects.InspObj.prototype.addPoint = function (p, scene) {//添加巡查点
    var mesh = new THREE.Mesh(new THREE.SphereGeometry(Constants.INSPECTION_POINT_SIZE), new THREE.MeshPhongMaterial({color: "red"}));
    mesh.position.set(p.x, p.y, p.z);
    this.points.push(mesh);
    scene.add(mesh);
}

HWObjects.InspObj.prototype.removePoint = function (scene) {//移除巡查点
    scene.remove(this.points[this.points.length - 1]);
    this.points.splice(this.points.length - 1, 1);
}

HWObjects.InspObj.prototype.clearPoints = function (scene) {
    for (var i = 0; i < this.points.length; i++) scene.remove(this.points[i]);
    this.points = [];
}


//BIM Object
HWObjects.BIMObj = function () {

    this.materials = [];
    this.materials.push(new THREE.MeshPhongMaterial({color: "blue", side: 2}));
    this.MATERIAL_SELECTED = 0;
    this.materials.push(new THREE.MeshPhongMaterial({visible: false}));
    this.MATERIAL_HIDDEN = 1;

    this.CONST_MATERIAL_NUM = this.materials.length;
    this.materials.side = THREE.DoubleSide;

    this.object = new THREE.Mesh(new THREE.Geometry(), null);
    this.logicObjs = [];
    /*
    {
        boundingSpehere: ,
        faces: []
    }
     */

    this.loader = new THREE.FileLoader();
    this.loader.setResponseType("arraybuffer");

    this.test = [];

    this.loadMaterials();
    this.loadBoundingSpheres();
}

HWObjects.BIMObj.prototype.intersect = function () {
    var raycaster = new THREE.Raycaster();
    return function (mouse, camera) {
        raycaster.setFromCamera(mouse, camera);

        var time=Date.now();

           var intersects = raycaster.intersectObject(this.object);

           console.log("intersect time",Date.now()-time)

           if (intersects.length == 0) return null;
           for (var i = 0; i < intersects.length; i++) {
               if (intersects[i].face.materialIndex != this.MATERIAL_HIDDEN &&
                   intersects[i].face.materialIndex != intersects[i].face._materialIndex + this.MATERIAL_TRANSLUCENT_OFFSET)
                   return intersects[i];
           }
           return null;

    /*    var intersects = this.fastIntersect(raycaster.ray);
        console.log("intersect time",Date.now()-time)
        if (intersects.length == 0) return null;
        return intersects[0];*/
    }
}();

HWObjects.BIMObj.prototype.fastIntersect = function (ray) {
    var tmpFaces = [];
    for (var ifcId in this.logicObjs) {
        var obj = this.logicObjs[ifcId];
        if (obj.faces[0].materialIndex != this.MATERIAL_HIDDEN && obj.faces[0].materialIndex != obj.faces[0]._materialIndex + this.MATERIAL_TRANSLUCENT_OFFSET) {
            if (ray.intersectSphere(obj.boundingSphere) != null) {
                for (var i = 0; i < obj.faces.length; i++)
                    tmpFaces.push(obj.faces[i]);
            }
        }
    }
    var intersects = [];
    var vertices = this.object.geometry.vertices;
    for (var i = 0; i < tmpFaces.length; i++) {
        var face = tmpFaces[i];
        var target = new THREE.Vector3();
        if (ray.intersectTriangle(vertices[face.a], vertices[face.b], vertices[face.c], false, target)) {
            intersects.push({
                face: face,
                point: target,
                distance: ray.origin.distanceTo(target)
            });
        }
    }
    intersects.sort(IntersectCmp);
    return intersects;
}

function IntersectCmp(a, b) {
    return a.distance - b.distance;
}

HWObjects.BIMObj.prototype.hideRestMeshes = function (__ifcId) {
    for (var ifcId in this.logicObjs) {//重写
        if (ifcId == __ifcId) {//类型不同，数值相同
            continue;
        }
        this.showOrHideMesh(0, [ifcId], false, null);
    }
}

HWObjects.BIMObj.prototype.translRestMeshes = function (__ifcId) {
    for (var ifcId in this.logicObjs) {//重写
        if (ifcId == __ifcId) {//类型不同，数值相同
            continue;
        }
        this.translMesh(ifcId);
    }
}
HWObjects.BIMObj.prototype.showAllMeshes = function () {
    for (var ifcId in this.logicObjs) {
        this.showOrHideMesh(0, [ifcId], true, null);
    }
}

HWObjects.BIMObj.prototype.highlightMesh = function (ifcId) {
    var self = this;
    this.traverseFacesByIfcId(ifcId, function (face) {
        if (face.materialIndex == self.MATERIAL_SELECTED) {
            face.materialIndex = face._materialIndex;
        } else {
            face.materialIndex = self.MATERIAL_SELECTED;
        }
    })
    self.object.geometry.groupsNeedUpdate = true;
}

HWObjects.BIMObj.prototype.translMesh = function (ifcId) {
    var self = this;
    self.traverseFacesByIfcId(ifcId, function (face) {//face instanceof THREE.Face3
        face.materialIndex = face._materialIndex + self.MATERIAL_TRANSLUCENT_OFFSET;
    });
    self.object.geometry.groupsNeedUpdate = true;
}

HWObjects.BIMObj.prototype.showOrHideMesh = function (fileId, ifcIds, visib, scene) {

    if (this.logicObjs[ifcIds[0]] != null) {//存在
        var self = this;
        for (var i = 0, il = ifcIds.length; i < il; i++) {

            self.traverseFacesByIfcId(ifcIds[i], function (face) {//face instanceof THREE.Face3
                if (visib) {
                    face.materialIndex = face._materialIndex;
                } else {
                    face.materialIndex = self.MATERIAL_HIDDEN;
                }
            });
            self.object.geometry.groupsNeedUpdate = true;
        }
    } else {
        this.loadMesh(fileId, scene);
    }
}

HWObjects.BIMObj.prototype.traverseFacesByIfcId = function (ifcId, operation) {
    var faces = this.logicObjs[ifcId].faces;
    for (var j = 0; j < faces.length; j++) {
        operation(faces[j]);
    }
}

HWObjects.BIMObj.prototype.loadBoundingSpheres = function (callback) {
    var self = this;
    if (self.boundingSpheres != null) {
        if (callback != null)
            callback(self.boundingSpheres);
        return;
    }
    $.get(Constants.resourcesURL.bounding_spheres, function (data, status) {
        self.boundingSpheres = [];
        for (var ifcId in data) {
            var per = data[ifcId];
            self.boundingSpheres[ifcId] = new THREE.Sphere(new THREE.Vector3(per.centerX, per.centerY, per.centerZ), per.radius);
        }
        data = null;
        if (callback != null)
            callback(self.boundingSpheres);
    });
}

HWObjects.BIMObj.prototype.loadMaterials = function () {
    var self = this;

    this.loader.load(Constants.resourcesURL.material + 'm.hwbin', function (data) {
        var binLoader = new BinDataLoader(data);

        var mtlAmount = binLoader.readInt(1)[0];
        self.MATERIAL_TRANSLUCENT_OFFSET = mtlAmount;
        for (var i = 0; i < mtlAmount; i++) {
            var int_3 = binLoader.readInt(3);//color specular shininess
            var float_1 = binLoader.readFloat(1);//opacity

            var srcIndex = i + self.CONST_MATERIAL_NUM;
            self.materials[srcIndex] = new THREE.MeshPhongMaterial({
                color: int_3[0],
                specular: int_3[1],
                shininess: int_3[2],
                opacity: float_1[0],
                side: THREE.DoubleSide,
                transparent: (float_1[0] < 1.0),
            });

            //半透明
            var translucentIdx = srcIndex + self.MATERIAL_TRANSLUCENT_OFFSET;
            self.materials[translucentIdx] = self.materials[srcIndex].clone();
            if (self.materials[translucentIdx].opacity > Constants.TRANSLUCENT)
                self.materials[translucentIdx].opacity = Constants.TRANSLUCENT;
            else self.materials[translucentIdx].opacity = self.materials[translucentIdx].opacity / 2;
            self.materials[translucentIdx].transparent = true;
        }

        binLoader.close();
        int_3 = null;
        float_1 = null;

        self.object.material = self.materials;
    })
}

HWObjects.BIMObj.prototype.loadMesh = function (fId, scene) {
    var self = this;
    this.loader.load(Constants.resourcesURL.mesh + 'm' + fId + "l" + Constants.LOD + ".hwbin", function (data) {


        if (self.objectInScene==null){
            scene.add(self.object);
            self.objectInScene=true;
            console.log("insert ");
        }

        var geometry = self.object.geometry;
        var verticesOffset = geometry.vertices.length;

        var binLoader = new BinDataLoader(data);

        var vNum = binLoader.readInt(1)[0];
        var fNum = binLoader.readInt(1)[0];
        var gNum = binLoader.readInt(1)[0];

        var vertices = binLoader.readFloat(vNum);
        var faces;

        if (vNum < 65536)
            faces = binLoader.readShort(fNum);
        else faces = binLoader.readInt(fNum);

        for (var i = 0; i < vertices.length; i += 3) {
            geometry.vertices.push(new THREE.Vector3(vertices[i], vertices[i + 1], vertices[i + 2]));
        }

        var indStart = 0;

        for (var g = 0; g < gNum; g++) {
            var ifcId = binLoader.readInt(1)[0];
            var count = binLoader.readInt(1)[0];
            var mtlId = binLoader.readInt(1)[0] + self.CONST_MATERIAL_NUM;//

            var indEnd = indStart + count;

            if (self.logicObjs[ifcId] == null) {
                self.logicObjs[ifcId] = {
                    boundingSphere: self.boundingSpheres[ifcId],
                    faces: []
                };
            }

            for (var i = indStart; i < indEnd; i += 3) {
                var sa = faces[i],
                    sb = faces[i + 1],
                    sc = faces[i + 2];
                var face = new THREE.Face3(sa + verticesOffset, sb + verticesOffset, sc + verticesOffset,
                    null, null, mtlId);
                face._materialIndex = mtlId;
                face.ifcId = ifcId;

                self.logicObjs[ifcId].faces.push(face);
                geometry.faces.push(face);
            }


            indStart += count;
        }

        geometry.computeFaceNormals();

        vertices = null;
        faces = null;

        binLoader.close();
        binLoader = null;

        geometry.verticesNeedUpdate = true;
        geometry.elementsNeedUpdate = true;

    });
}

HWObjects.BIMObj.prototype.myComputeBoundingSphere = function () {
    var position = this.attributes.position.array;
    var index = this.index.array;

    this.boundingSphere = new THREE.Sphere();
    var center = this.boundingSphere.center;

    var min = new THREE.Vector3(Infinity, Infinity, Infinity);
    var max = new THREE.Vector3(-Infinity, -Infinity, -Infinity);
    for (var i = 0, il = index.length; i < il; i++) {
        var idx = index[i];
        var x = position[idx * 3 + 0];
        var y = position[idx * 3 + 1];
        var z = position[idx * 3 + 2];

        min.x = Math.min(min.x, x);
        min.y = Math.min(min.y, y);
        min.z = Math.min(min.z, z);

        max.x = Math.max(max.x, x);
        max.y = Math.max(max.y, y);
        max.z = Math.max(max.z, z);
    }

    center.addVectors(min, max).multiplyScalar(0.5);

    var mxDis = 0;
    for (var i = 0, il = index.length; i < il; i++) {
        var idx = index[i];
        var x = position[idx * 3 + 0];
        var y = position[idx * 3 + 1];
        var z = position[idx * 3 + 2];

        mxDis = Math.max(mxDis, Math.pow((x - center.x), 2) + Math.pow((y - center.y), 2) + Math.pow((z - center.z), 2));
    }

    this.boundingSphere.radius = Math.sqrt(mxDis);
}

HWObjects.BIMObj.prototype.loadOctree = function (callback) {
    var self = this;
    this.loader.load(Constants.resourcesURL.octree + "t.hwbin", function (data) {
        var nodes = [];
        var binLoader = new BinDataLoader(data);
        var dataAmount = binLoader.readInt(1)[0];
        var _mat = self.materials[0];
        for (var i = 0; i < dataAmount; i++) {
            var k = binLoader.readInt(1)[0];//k ~ k+7
            var d = binLoader.readInt(1)[0];//d个数据
            var data = binLoader.readInt(d);

            var box = binLoader.readFloat(6);

            var minX = box[0], minY = box[1], minZ = box[2];
            var maxX = box[3], maxY = box[4], maxZ = box[5];
            var boundingBox = new THREE.Mesh(new THREE.BoxGeometry(maxX - minX, maxY - minY, maxZ - minZ, 1, 1, 1), _mat);
            boundingBox.position.set((minX + maxX) / 2, (minY + maxY) / 2, (minZ + maxZ) / 2);
            boundingBox.geometry.computeBoundingSphere();
            nodes[i] = {
                boundingBoxMesh: boundingBox,
                subTrees: k,

                minX: minX,
                maxX: maxX,
                minY: minY,
                maxY: maxY,
                minZ: minZ,
                maxZ: maxZ,

                data: []
            };
        }
        for (var i = 0; i < dataAmount; i++) {//更新子树
            var k = nodes[i].subTrees;
            if (k == 0) nodes[i].subTrees = null;
            else {
                nodes[i].subTrees = [];
                for (var j = k; j < k + 8; j++)
                    nodes[i].subTrees[j - k] = nodes[j];
            }
        }
        self.octree = nodes[0];//根节点

        if (callback)
            callback(nodes[0]);
    });
}

//Data Loader
function BinDataLoader(data) {
    this.data = data;
    this.dOffset = 0;
}

Object.assign(BinDataLoader.prototype, {
    readShort: function (cnt) {
        var tmp = new Uint16Array(this.data, this.dOffset, cnt);
        this.dOffset += cnt * 2;
        return tmp;
    },
    readInt: function (cnt) {
        var tmp = new Uint32Array(this.data, this.dOffset, cnt);
        this.dOffset += cnt * 4;
        return tmp;
    },
    readFloat: function (cnt) {
        var tmp = new Float32Array(this.data, this.dOffset, cnt);
        this.dOffset += cnt * 4;
        return tmp;
    },
    close: function () {
        this.data = null;
    }
})
