function ODoms(){
    this.propertyView=document.getElementById("property");
}
ODoms.prototype.update_propertyView=function(ifcId){
    var self=this;
    $.get(Constants.resourcesURL['ifc_attr'] + ifcId, function (data, status) {//获取属性

        while (self.propertyView.rows.length > 0) self.propertyView.deleteRow(0);
        if (data == undefined || data == null) return;
        for (var i in data) {
            var row = self.propertyView.insertRow(self.propertyView.rows.length);
            var c1 = row.insertCell(0);
            c1.innerHTML = i;
            var c2 = row.insertCell(1);
            c2.innerHTML = data[i];
        }
    });

}
