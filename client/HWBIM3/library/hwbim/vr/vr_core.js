function VRCore(){
    this.view=new VRBIMView(document.getElementById("bim_view"));

    this.objects=new VRHWObjects();

    this.loadGround();

  //  this.loadBox();

    this.loadWholeBuilding();
}

VRCore.prototype.loadBox=function(){
    var box=new THREE.Mesh(new THREE.BoxGeometry(5,4,3),new THREE.MeshPhongMaterial({color:"blue"}));
    this.view.scene.add(box);
}

VRCore.prototype.loadGround=function(){
    // Create floor
    var floorTexture = THREE.ImageUtils.loadTexture('img/grass.jpg');
    floorTexture.wrapS = THREE.RepeatWrapping;
    floorTexture.wrapT = THREE.RepeatWrapping;
    floorTexture.repeat = new THREE.Vector2(50, 50);
    var floorMaterial = new THREE.MeshPhongMaterial({
        map: floorTexture
    });
    var floorGeometry = new THREE.PlaneBufferGeometry(1000, 1000);
    var floor = new THREE.Mesh(floorGeometry, floorMaterial);
    floor.rotation.x = -Math.PI / 2;
    this.view.scene.add(floor);
}

VRCore.prototype.loadWholeBuilding=function(){
    var self=this;
    $.get(Constants.resourcesURL.construct_tree,function(data, status){
        //找到最大的fileId
        var mxFileId;
        var sData=JSON.stringify(data);
        var start=sData.lastIndexOf(':')+1;
        var end=sData.length-9;
        var sFileId=sData.substring(start,end);
        mxFileId=parseInt(sFileId);

        var finishCnt=0;
        for(var i=0;i<mxFileId;i++){
            self.objects.bimObj.loadMesh(i, self.view.scene, function(){
                finishCnt++;
                if(finishCnt==mxFileId){
                    self.objects.bimObj.object.geometry.rotateX(-Math.PI/2);
                    self.loadOctree();
                }
            });
        }
    });
}

VRCore.prototype.loadOctree = function () {
    var self = this;
    this.objects.bimObj.loadOctree(function (root) {
        //设置相机

        var center=root.boundingBoxMesh.position;
        var radius = root.boundingBoxMesh.geometry.boundingSphere.radius;

        console.log(center,radius);

        var pos=self.objects.bimObj.object.position;

        pos.y=Constants.VR_CAMERA_Y+center.z;


        //设置光源
        self.view.dirLight1.position.set(root.maxX * 10, root.maxY * 10, root.maxZ * 10);
    });
}
