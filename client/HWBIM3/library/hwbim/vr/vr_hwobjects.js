function VRHWObjects() {
    this.bimObj = new VRHWObjects.BIMObj();
}

//BIM Object
VRHWObjects.BIMObj = function () {

    this.materials = [];
    this.materials.push(new THREE.MeshPhongMaterial({color: "blue", side: 2}));
    this.MATERIAL_SELECTED = 0;
    this.materials.push(new THREE.MeshPhongMaterial({visible: false}));
    this.MATERIAL_HIDDEN = 1;

    this.CONST_MATERIAL_NUM = this.materials.length;
    this.materials.side = THREE.DoubleSide;

    this.object = new THREE.Mesh(new THREE.Geometry(), null);


    this.loader = new THREE.FileLoader();
    this.loader.setResponseType("arraybuffer");

    this.loadMaterials();
}

VRHWObjects.BIMObj.prototype.loadMaterials = function () {
    var self = this;

    this.loader.load(Constants.resourcesURL.material + 'm.hwbin', function (data) {
        var binLoader = new BinDataLoader(data);

        var mtlAmount = binLoader.readInt(1)[0];
        self.MATERIAL_TRANSLUCENT_OFFSET = mtlAmount;
        for (var i = 0; i < mtlAmount; i++) {
            var int_3 = binLoader.readInt(3);//color specular shininess
            var float_1 = binLoader.readFloat(1);//opacity

            var srcIndex = i + self.CONST_MATERIAL_NUM;
            self.materials[srcIndex]=new THREE.MeshPhongMaterial({
                color: int_3[0],
                specular: int_3[1],
                shininess: int_3[2],
                opacity: float_1[0],
                side: THREE.DoubleSide,
                transparent: (float_1[0] < 1.0),
            });

            //半透明
            var translucentIdx=srcIndex+self.MATERIAL_TRANSLUCENT_OFFSET;
            self.materials[translucentIdx]= self.materials[srcIndex].clone();
            if(self.materials[translucentIdx].opacity>Constants.TRANSLUCENT)
                self.materials[translucentIdx].opacity=Constants.TRANSLUCENT;
            else self.materials[translucentIdx].opacity=self.materials[translucentIdx].opacity/2;
            self.materials[translucentIdx].transparent=true;
        }

        binLoader.close();
        int_3 = null;
        float_1 = null;

        self.object.material = self.materials;
    })
}

VRHWObjects.BIMObj.prototype.loadMesh = function (fId, scene, callback) {
    var self = this;
    var inserted = false;
    this.loader.load(Constants.resourcesURL.mesh + 'm' + fId + "l" + Constants.LOD + ".hwbin", function (data) {

        if (!inserted) scene.add(self.object);

        var geometry = self.object.geometry;
        var verticesOffset = geometry.vertices.length;

        var binLoader = new BinDataLoader(data);

        var vNum = binLoader.readInt(1)[0];
        var fNum = binLoader.readInt(1)[0];
        var gNum = binLoader.readInt(1)[0];

        var vertices = binLoader.readFloat(vNum);
        var faces;

        if(vNum<65536)
            faces = binLoader.readShort(fNum);
        else faces=binLoader.readInt(fNum);

        for (var i = 0; i < vertices.length; i += 3) {
            geometry.vertices.push(new THREE.Vector3(vertices[i], vertices[i + 1], vertices[i + 2]));
        }

        var indStart = 0;

        for (var g = 0; g < gNum; g++) {
            var ifcId = binLoader.readInt(1)[0];
            var count = binLoader.readInt(1)[0];
            var mtlId = binLoader.readInt(1)[0] + self.CONST_MATERIAL_NUM;//

            var indEnd = indStart + count;

            for (var i = indStart; i < indEnd; i += 3) {
                var sa = faces[i],
                    sb = faces[i + 1],
                    sc = faces[i + 2];
                var face = new THREE.Face3(sa + verticesOffset, sb + verticesOffset, sc + verticesOffset,
                    null, null, mtlId);
                face._materialIndex = mtlId;
                face.ifcId = ifcId;

                geometry.faces.push(face);
            }


            indStart += count;
        }

        geometry.computeFaceNormals();

        vertices = null;
        faces = null;

        binLoader.close();
        binLoader = null;

        geometry.verticesNeedUpdate = true;
        geometry.elementsNeedUpdate = true;

        if(callback!=null)
            callback();
    });
}

VRHWObjects.BIMObj.prototype.myComputeBoundingSphere = function () {
    var position = this.attributes.position.array;
    var index = this.index.array;

    this.boundingSphere = new THREE.Sphere();
    var center = this.boundingSphere.center;

    var min = new THREE.Vector3(Infinity, Infinity, Infinity);
    var max = new THREE.Vector3(-Infinity, -Infinity, -Infinity);
    for (var i = 0, il = index.length; i < il; i++) {
        var idx = index[i];
        var x = position[idx * 3 + 0];
        var y = position[idx * 3 + 1];
        var z = position[idx * 3 + 2];

        min.x = Math.min(min.x, x);
        min.y = Math.min(min.y, y);
        min.z = Math.min(min.z, z);

        max.x = Math.max(max.x, x);
        max.y = Math.max(max.y, y);
        max.z = Math.max(max.z, z);
    }

    center.addVectors(min, max).multiplyScalar(0.5);

    var mxDis = 0;
    for (var i = 0, il = index.length; i < il; i++) {
        var idx = index[i];
        var x = position[idx * 3 + 0];
        var y = position[idx * 3 + 1];
        var z = position[idx * 3 + 2];

        mxDis = Math.max(mxDis, Math.pow((x - center.x), 2) + Math.pow((y - center.y), 2) + Math.pow((z - center.z), 2));
    }

    this.boundingSphere.radius = Math.sqrt(mxDis);
}

VRHWObjects.BIMObj.prototype.loadOctree = function (callback) {
    var self = this;
    this.loader.load(Constants.resourcesURL.octree + "t.hwbin", function (data) {
        var nodes = [];
        var binLoader = new BinDataLoader(data);
        var dataAmount = binLoader.readInt(1)[0];
        var _mat = self.materials[0];
        for (var i = 0; i < dataAmount; i++) {
            var k = binLoader.readInt(1)[0];//k ~ k+7
            var d = binLoader.readInt(1)[0];//d个数据
            var data = binLoader.readInt(d);

            var box = binLoader.readFloat(6);

            var minX = box[0], minY = box[1], minZ = box[2];
            var maxX = box[3], maxY = box[4], maxZ = box[5];
            var boundingBox = new THREE.Mesh(new THREE.BoxGeometry(maxX - minX, maxY - minY, maxZ - minZ, 1, 1, 1), _mat);
            boundingBox.position.set((minX + maxX) / 2, (minY + maxY) / 2, (minZ + maxZ) / 2);
            boundingBox.geometry.computeBoundingSphere();
            nodes[i] = {
                boundingBoxMesh: boundingBox,
                subTrees: k,

                minX: minX,
                maxX: maxX,
                minY: minY,
                maxY: maxY,
                minZ: minZ,
                maxZ: maxZ,

                data: []
            };
        }
        for (var i = 0; i < dataAmount; i++) {//更新子树
            var k = nodes[i].subTrees;
            if (k == 0) nodes[i].subTrees = null;
            else {
                nodes[i].subTrees = [];
                for (var j = k; j < k + 8; j++)
                    nodes[i].subTrees[j - k] = nodes[j];
            }
        }
        self.octree = nodes[0];//根节点

        if (callback)
            callback(nodes[0]);
    });
}

//Data Loader
function BinDataLoader(data) {
    this.data = data;
    this.dOffset = 0;
}

Object.assign(BinDataLoader.prototype, {
    readShort: function (cnt) {
        var tmp = new Uint16Array(this.data, this.dOffset, cnt);
        this.dOffset += cnt * 2;
        return tmp;
    },
    readInt: function (cnt) {
        var tmp = new Uint32Array(this.data, this.dOffset, cnt);
        this.dOffset += cnt * 4;
        return tmp;
    },
    readFloat: function (cnt) {
        var tmp = new Float32Array(this.data, this.dOffset, cnt);
        this.dOffset += cnt * 4;
        return tmp;
    },
    close: function () {
        this.data = null;
    }
})
