
function VRBIMView(container){
    this.moving=false;
    this.forward=false;
    this.container=container;
    this.initThree();
    this.addListener();
}

VRBIMView.prototype.addListener=function(){
    var self=this;
    this.container.addEventListener("mousedown",function(e){
        self.moving=true;
        if(e.x<self.cWidth/2){//屏幕左侧
            self.forward=true;
        }else{//屏幕右侧
            self.forward=false;
        }
    });
    this.container.addEventListener("mouseup",function(e){
        self.moving=false;
    });
}


VRBIMView.prototype.initThree=function(){
    this.renderer=new THREE.WebGLRenderer({antialias:true});
    this.renderer.setClearColor(0x000000);
    this.container.appendChild(this.renderer.domElement);

    this.container.appendChild(WEBVR.createButton(this.renderer));

    this.effect=new THREE.VREffect(this.renderer);
    //   this.effect=new THREE.StereoEffect(this.renderer);


    this.scene=new THREE.Scene();
    this.camera=new THREE.PerspectiveCamera(90,1,0.001,700);
    this.camera.lookAt(new THREE.Vector3());
    this.camera.position.set(0, 15, 0);
   // this.camera.position.set(0, Constants.VR_CAMERA_Y, 0);


    //  this.controls=new THREE.DeviceOrientationControls(this.camera);
    // this.controls.connect();
    //   this.controls.update();

    this.vr_controls=new THREE.VRControls(this.camera);
    this.vr_controls.standing=true;

    var params = {
        hideButton: false, // Default: false.
        isUndistorted: false // Default: false.
    };
    this.manager = new WebVRManager(this.renderer, this.effect, this.params);

    //init lights
    this.dirLight1=new THREE.SpotLight(0x999999, 1);
    this.dirLight1.position.set(1,1,1);
    this.scene.add(this.dirLight1);
    this.ambLight1=new THREE.AmbientLight(0xaaaaaa);
    this.ambLight1.position.set(-1,-1,1);
    this.scene.add(this.ambLight1);

    this.onContainerResize();
    this.addContainerResizeListener();

    this.startRender();
}

VRBIMView.prototype.addContainerResizeListener=function(){
    var self=this;
    window.addEventListener("resize", function(){
        self.onContainerResize();
    });
}

VRBIMView.prototype.onContainerResize=function(){
    this.cWidth=this.container.clientWidth;
    this.cHeight=this.container.clientHeight;
    this.camera.aspect=this.cWidth/this.cHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.cWidth,this.cHeight);
    this.effect.setSize(this.cWidth,this.cHeight);
}

VRBIMView.prototype.startRender=function(){
    var self=this;
    function render(){
        requestAnimationFrame(render);

        //   if(self.effect!=null)
        //      self.effect.render(self.scene,self.camera);

        self.manager.render(self.scene, self.camera,20);

        /*    if(self.controls !=null) {
                self.controls.update();
            }*/

        if(self.vr_controls!=null){
            self.vr_controls.update();
        }

        if(self.moving) {
            var dir;
            if(self.forward) dir=1;
            else dir=-1;

            var camera = self.camera;
            var dir = camera.getWorldDirection();
            var off = dir.clone();
            off.multiplyScalar(Constants.VR_CAMERA_SPEED);
            if(!self.forward)
                off.multiplyScalar(-1);
            camera.position.add(off);
        }
    };

    render();
}

