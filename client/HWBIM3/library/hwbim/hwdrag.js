function Hwdrag(whole, title, close, resize, body){
    var w=document.getElementById(whole);
    var t=document.getElementById(title);
    var c=document.getElementById(close);
    var r=document.getElementById(resize);
    var b=document.getElementById(body);


    b.style.height=(w.clientHeight-t.clientHeight)+"px";
    var bDrag = false;
    var disX=0, disY = 0;
    t.onmousedown = function (event)
    {
        var event = event || window.event;
        bDrag = true;
        disX = event.clientX - w.offsetLeft;
        disY = event.clientY - w.offsetTop;
        return false
    };
    window.addEventListener("mousemove",function (event)
    {
        if (!bDrag) return;
        var event = event || window.event;
        var iL = event.clientX - disX;
        var iT = event.clientY - disY;
        var maxL = document.documentElement.clientWidth - w.offsetWidth;
        var maxT = document.documentElement.clientHeight - w.offsetHeight;

        iL = iL < 0 ? 0 : iL;
        iL = iL > maxL ? maxL : iL;
        iT = iT < 0 ? 0 : iT;
        iT = iT > maxT ? maxT : iT;
        w.style.marginTop = w.style.marginLeft = 0;
        w.style.left = iL + "px";
        w.style.top = iT + "px";
        return false
    });
    window.addEventListener("mouseup",function ()
    {
        bDrag = false;
    });
    t.onmouseup= function ()
    {
        console.log("up");
        bDrag = false;
    };
    c.onmouseup=function(){
        w.style.display="none";
    }
    var resize=false;
    r.onmousedown=function(){
        resize=true;
    };
    window.addEventListener("mousemove",function(event){
        if(!resize) return ;
        console.log("move");
        var wid=event.clientX- w.offsetLeft;
        var hei=event.clientY- w.offsetTop;

        wid=wid<100?100:wid;
        hei=hei<100?100:hei;
        wid+=5;
        hei+=5;
        w.style.width = wid + "px";
        w.style.height = hei + "px";

        b.style.height=(w.clientHeight-t.clientHeight)+"px";
    });
    window.addEventListener("mouseup",function () {
        resize=false;
    });

}