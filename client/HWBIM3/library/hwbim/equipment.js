function Equipments(data){
    this.equips={}
    /*
    ifcId: equipment
     */
    this.init(data);
    this.startRequestDataLoop();
}

Equipments.prototype.init=function(data){
    if(data==null) return;
    for(var i=0;i<data.length;i++){
        var equipType=data[i];
        var equipName=equipType.name;

        var equipList=equipType.children;
        for(var j=0;j<equipList.length;j++){
            var equip=equipList[j];
            equip.equipType=equipName;
            this.addEquipmentProto(equip);
            this.equips[equip.ifcId]=equip;
        }
    }
}

Equipments.prototype.loadBoundingSpheres=function(logicObjs, view){
    for(var ifcId in this.equips){
        this.equips[ifcId].boundingSphere=logicObjs[ifcId];

        if(this.equips[ifcId].equipType=='火焰探测器1'){
            this.equips[ifcId].fire=new Fire(view.renderer, view.scene, view.camera, this.equips[ifcId].boundingSphere.center);
        }
    }
    console.log(this.equips);
}



Equipments.prototype.addEquipmentProto=function(equipment){

    //创建element
    var dom=document.createElement("div");
    document.body.appendChild(dom);

    dom.setAttribute("class","equip_panel");
    dom.style.position="absolute";
    dom.style.visibility="hidden";

    equipment.dom=dom;

    equipment.setData=function(data){
        var content="equipment id"+Constants.EQUIP_PANEL_QUOT+this.equipId+"<br>";
        for(var key in data){
            content+=key+Constants.EQUIP_PANEL_QUOT+data[key]+"<br>";
        }
        if(this.equipType=='火焰探测器1'){
            if(this.fire){
                if(data['fire']!=null)
                    this.fire.showFire(data['fire']);
                else{
                    this.fire.showFire(false);
                }
            }
        }
        this.dom.innerHTML=content;
    }

    equipment.show=function(){
        this.dom.style.visibility="visible";
    }

    equipment.hide=function(){
        this.dom.style.visibility="hidden";
    }

    equipment.updateData=function(data){
        var self=this;
        $.get(Constants.resourcesURL.equip_data+this.equipId,function(data,status){
            self.setData(data);
        })
    }

    equipment.updateDomPos=function(pos){
        this.dom.style.left=pos.x+"px";
        this.dom.style.top=pos.y+"px";
    }
}

Equipments.prototype.startRequestDataLoop=function(){
    var self=this;
    var loop=function(){
        for(var ifcId in self.equips){
            self.equips[ifcId].updateData();
        }
    }
    setInterval(function(){
        loop();
    },Constants.UPDATE_EQUIPMENT_DATA_DELTA_TIME);
}

Equipments.prototype.userClick=function(__ifcId){//用户点击一个模型，传入ifcId，和几何信息
    this.hideAllEquipDom();
    if(this.equips[__ifcId]!=null){
        this.equips[__ifcId].show();
    }
}

Equipments.prototype.updateDomPosition=function(camera, width, height){
    for(var ifcId in this.equips){
        if(this.equips[ifcId].boundingSphere==null) continue;
        var pos=this.equips[ifcId].boundingSphere.center;
        var screenPos=this.pointToScreenPosition(pos, camera, width, height);
        this.equips[ifcId].updateDomPos(screenPos);
    }
}

Equipments.prototype.hideAllEquipDom=function(){
    for(var ifcId in this.equips){
        this.equips[ifcId].hide();
    }
}

Equipments.prototype.pointToScreenPosition=function(p, camera, width, height) {
    var p = new THREE.Vector3(p.x, p.y, p.z);
    var vector = p.project(camera);
    vector.x = (vector.x + 1) / 2 * width;
    vector.y = -(vector.y - 1) / 2 * height;
    return vector;
}

Equipments.prototype.inspectEquips=function(manPos){
    this.hideAllEquipDom();
    for(var ifcId in this.equips){
        if(this.equips[ifcId]==null) continue;
        if(this.equips[ifcId].boundingSphere==null) continue;

        var center=this.equips[ifcId].boundingSphere.center;
        var radius=this.equips[ifcId].boundingSphere.radius;

        if(manPos.distanceTo(center)-radius<Constants.INSPECTION_DIS_TO_SHOW_DATA){
            this.equips[ifcId].show();
            console.log(ifcId);
        }
    }
}