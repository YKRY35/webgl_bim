function InspMan(scene) {
    this.clock = new THREE.Clock();
    this.manVel=Constants.INSPECTION_MAN_VELOCITY;
    this.loadModel(scene);
}

InspMan.prototype.loadModel = function (scene) {
    var self = this;
    new THREE.ObjectLoader().load('models/marine_anims_core.json', function (loadedObject) {
        loadedObject.traverse(function (child) {
            if (child instanceof THREE.SkinnedMesh) {
                self.obj = child;
            }
        });
        scene.add(self.obj);
        self.obj.scale.x = self.obj.scale.y = self.obj.scale.z = 0.0075;

        self.setVisibility(false);

        self.mixer = new THREE.AnimationMixer(self.obj);
        self.walkAction = self.mixer.clipAction('walk');
        self.walkAction.setEffectiveTimeScale(1);
        self.walkAction.setEffectiveWeight(1);
        self.walkAction.enabled = true;
        self.walkAction.play();
    });
}

InspMan.prototype.update = function () {
    if (this.mixer)
        this.mixer.update(this.clock.getDelta());
}

InspMan.prototype.setDirection = function (angleZ) {//先绕确定方向，再转x坐标轴
    if (this.obj == null) return;//未加载

    this.obj.rotation.set(Math.PI / 2, 0, angleZ-Math.PI/2, "ZXY");

}

InspMan.prototype.update = function () {
    var delta = this.clock.getDelta();
    THREE.AnimationHandler.update(delta);
    this.mixer.update(delta);
}

InspMan.prototype.setVisibility = function (vis) {
    this.obj.visible = vis;
}

InspMan.prototype.setPosition = function (p) {
    this.obj.position.set(p.x, p.y, p.z);
}

InspMan.prototype.getPosition = function () {
    return this.obj.position;
}

InspMan.prototype.goThere = function (tarPos) {//去目标点tp
    var manPos = this.obj.position;

    var distance = manPos.distanceTo(tarPos);

    if (distance < 0.000001) return true;

    var dir = new THREE.Vector3();
    dir.subVectors(tarPos, manPos).normalize();//人物到目标点的方向

    var offset = dir.clone();
    offset.multiplyScalar(this.manVel);//人物这一帧的位移

    //设置朝向
    var dirProj = dir.clone();
    dirProj.z = 0;//方向再xy平面的投影
    var manDir = Math.atan2(dir.y, dir.x);
    this.setDirection(manDir);

    if (offset.length() < distance) {//这一帧无法到达
        var fTarPos = new THREE.Vector3();
        fTarPos.addVectors(manPos, offset);
        this.setPosition(fTarPos);
        return false;
    } else {//直接到达终点
        this.setPosition(tarPos);
        return true;
    }
}