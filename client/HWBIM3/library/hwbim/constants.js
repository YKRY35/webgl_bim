var Constants={
    //View
    CAMERA_FOV: 40,

    INSPECTION_POINT_SIZE: 0.1,

    INSPECTION_MAN_VELOCITY: 0.05,

    UPDATE_EQUIPMENT_DATA_DELTA_TIME: 700,//ms

    EQUIP_PANEL_QUOT: "   ---   ",

    TRANSLUCENT: 0.1,

    INSPECTION_DIS_TO_SHOW_DATA: 3,

    LOD: 5,

    VR_CAMERA_Y: 15,

    VR_CAMERA_SPEED: 0.1,
}

initConstants=function(){
    var alias=GetQueryString("alias");
    var lod=GetQueryString("lod");
    if(lod!==null && lod !== ""){
        Constants.LOD=lod;
    }

 //   var BASE_URL= "http://120.78.150.162:35000/HWBIM/";
    var BASE_URL= "http://127.0.0.1:35000/HWBIM/";
    Constants.resourcesURL = {
        construct_tree: BASE_URL+"construct_tree.jsp?alias=" + alias,
        equipment_tree: BASE_URL+"equipment_tree.jsp?alias=" + alias,
        basic_data: BASE_URL+"basic_data.jsp?alias=" + alias,
        material: BASE_URL+ alias + '/decoded/material/',
        mesh: BASE_URL+ alias + '/decoded/mesh/',
        octree: BASE_URL + alias + '/decoded/octree/',
        ifc_attr: BASE_URL+'elem_attr.jsp?alias=' + alias + '&IfcId=',
        equip_data: BASE_URL+'get_equip_data.jsp?alias='+alias+'&id=',
        bounding_spheres: BASE_URL+'get_bounding_spheres.jsp?alias='+alias
    }
}

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg); //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}

initConstants();