
function BIMView(container){
    this.container=container;
    this.initThree();
}

BIMView.prototype.initThree=function(){
    this.renderer=new THREE.WebGLRenderer({antialias:true});
    this.renderer.setClearColor(0x000000);
    this.container.appendChild(this.renderer.domElement);

    this.scene=new THREE.Scene();
    this.camera=new THREE.PerspectiveCamera(Constants.CAMERA_FOV,1,1,1000);
    this.camera.up.set(0,0,1);
    this.controls=new THREE.OrbitControls(this.camera, this.container);


    //init lights
    this.dirLight1=new THREE.SpotLight(0x999999, 1);
    this.dirLight1.position.set(1,1,1);
    this.scene.add(this.dirLight1);
    this.ambLight1=new THREE.AmbientLight(0xaaaaaa);
    this.ambLight1.position.set(-1,-1,1);
    this.scene.add(this.ambLight1);

    var stats=new Stats();
    stats.domElement.style.position = "absolute";
    stats.domElement.style.left = "0px";
    stats.domElement.style.top = "0px";
    this.container.appendChild(stats.domElement);
    this.stats=stats;

    this.onContainerResize();
    this.addContainerResizeListener();

    this.startRender();
}

BIMView.prototype.addContainerResizeListener=function(){
    var self=this;
    window.addEventListener("resize", function(){
        self.onContainerResize();
    });
}

BIMView.prototype.onContainerResize=function(){
    this.cWidth=this.container.clientWidth;
    this.cHeight=this.container.clientHeight;
    this.camera.aspect=this.cWidth/this.cHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.cWidth,this.cHeight);
}

BIMView.prototype.startRender=function(){
    var self=this;
    function render(){
        requestAnimationFrame(render);

        self.renderer.clear();
        self.renderer.render(self.scene,self.camera);
        if(self.stats)
            self.stats.update();
    };
    render();
}

