function UController(view, objects) {
    this.view = view;
    this.objects = objects;
    this.oDoms = new ODoms();
    this.mode = {
        code: 0,
        switch: false,
        CODE_NORMAL: 0,
        CODE_INSP_SLCT: 1,//巡查选点
        CODE_INSP_ROAM: 2,//巡查漫游
    }
    this.initConstructTree();
    this.initEquipmentTree();
    this.initOtherDOMs();
    this.addMouseEvent();
    this.loadOctree();
    this.initInspectionClass();
}

UController.prototype.initOtherDOMs = function () {
    var self = this;
    $("#f_insp").click(function () {//点击巡查按钮
        self.mode.switch = true;
        self.mode.code = self.mode.CODE_INSP_SLCT;
    });
    $("#insp_backt").click(function () {//撤销点
        self.inspection.removePoint();
    });
    $("#insp_quit").click(function () {//点击结束巡查按钮
        self.view.controls.enabled = true;
        self.inspection.clearPoints();
        self.mode.switch = true;
        self.mode.code = self.mode.CODE_NORMAL;
        self.inspection.stopLoop();
    });
    $("#insp_start").click(function () {//点击开始巡查按钮
        self.view.controls.enabled = false;
        self.mode.switch = true;
        self.mode.code = self.mode.CODE_INSP_ROAM;
        self.inspection.startLoop();
    });

    //右键菜单
    context.init({
        fadeSpeed: 100,
        above: 'auto',
        preventDoubleContext: true,
        compress: false
    });
    context.attach("#bim_view", [
        {
            text: "隐藏选中构件",
            action: function () {
                self.hideSelectedMesh();
            }
        },
        {
            text: "隐藏其余构件",
            action: function () {
                self.hideRestMeshes();
            }
        },
        {
            text: "半透明选中构件",
            action: function () {
                self.translSelectedMesh();
            }
        },
        {
            text: "半透明其余构件",
            action: function () {
                self.translRestMeshes();
            }
        },
        {
            text: "显示所有控件",
            action: function () {
                self.showAllMeshes();
            }
        },
    ]);
}

UController.prototype.hideRestMeshes = function () {
    if(this.lstSelectedIfcId!=null) {
        this.objects.bimObj.highlightMesh(this.lstSelectedIfcId);
        this.objects.bimObj.hideRestMeshes(this.lstSelectedIfcId);
        this.lstSelectedIfcId = null;
    }
}

UController.prototype.translSelectedMesh=function(){
    if (this.lstSelectedIfcId !=null)
        this.objects.bimObj.translMesh(this.lstSelectedIfcId);
    this.lstSelectedIfcId=null;
}

UController.prototype.translRestMeshes=function(){
    if(this.lstSelectedIfcId!=null) {
        this.objects.bimObj.highlightMesh(this.lstSelectedIfcId);//取消选中
        this.objects.bimObj.translRestMeshes(this.lstSelectedIfcId);
        this.lstSelectedIfcId = null;
    }
}

UController.prototype.showAllMeshes = function () {
    this.objects.bimObj.showAllMeshes();
    this.lstSelectedIfcId = null;
}

UController.prototype.hideSelectedMesh = function () {
    if (this.lstSelectedIfcId !=null)
        this.objects.bimObj.showOrHideMesh(0, [this.lstSelectedIfcId], false, null);//隐藏
    this.lstSelectedIfcId = null;
}

UController.prototype.initInspectionClass = function () {
    var self = this;
    this.inspection = {
        points: [],//用户选点
        nPIndex: 0,//当前巡查点index
        tarPos: new THREE.Vector3,//巡查人员目标点
        loopFlag: false,
        inspMan: new InspMan(self.view.scene),
        addPoint: function (p) {//增加巡查点
            this.points.push(p);
            self.objects.inspObj.addPoint(p, self.view.scene);
        },
        removePoint: function () {
            this.points.splice(this.points.length - 1, 1);
            self.objects.inspObj.removePoint(self.view.scene);
        },
        clearPoints: function () {
            this.points = [];
            self.objects.inspObj.clearPoints(self.view.scene);
        },
        startLoop: function () {
            if (this.points.length < 2) return;
            this.inspMan.setVisibility(true);
            this.inspMan.setPosition(this.points[0]);
            this.nPIndex = 0;
            this.tarPos = this.points[1];
            this.loopFlag = true;
            var self = this;
            var loop = function () {
                if (self.loopFlag) {
                    requestAnimationFrame(loop);
                }
                self.loop();
            };
            loop();
        },
        stopLoop: function () {
            this.inspMan.setVisibility(false);
            this.loopFlag = false;
        },
        loop: function () {
            var finish = this.inspMan.goThere(this.tarPos);
            if (finish) {//到达当前目标点更新目标点
                this.nPIndex++;
                this.nPIndex %= this.points.length;
                this.tarPos = this.points[this.nPIndex];
            }
            this.inspMan.update();
            self.inspCamera.update(self.view.camera, this.inspMan.getPosition(), this.tarPos);

            self.equipments.updateDomPosition(self.view.camera, self.view.cWidth, self.view.cHeight);
            self.equipments.inspectEquips(this.inspMan.getPosition());
        }

    }

    this.inspCamera = {
        update: function (camera, manPos, tarPos) {


            var dir = new THREE.Vector3();
            dir.subVectors(tarPos, manPos).normalize();//人物到目标点的方向

            var manToCameraV = dir.clone();
            manToCameraV.multiplyScalar(-4.5);
            manToCameraV.z += 2.0;



            var tarCameraPos = new THREE.Vector3();
            tarCameraPos.addVectors(manToCameraV, manPos);//摄像机位置

            var cameraToFocusDis = tarCameraPos.distanceTo(manPos);

            var tCameraOppDir = new THREE.Vector3();
            tCameraOppDir.subVectors(tarCameraPos, manPos);//目标相机视角朝向的反向

            var nCameraOppDir = new THREE.Vector3();
            nCameraOppDir.subVectors(camera.position, manPos);//当前视角朝向的反向


            var sCameraOppDir=this.dirMoveTowards(nCameraOppDir, tCameraOppDir);//这一帧相机位置朝向反向

            var sCameraPos=sCameraOppDir.clone();
            sCameraPos.multiplyScalar(cameraToFocusDis);
            sCameraPos.add(manPos);//目标相机位置

            camera.position.set(sCameraPos.x,sCameraPos.y,sCameraPos.z);
            camera.lookAt(manPos);
            camera.updateProjectionMatrix();

        },
        MAX_ROT_ANGLE: 2.7 / 180 * Math.PI,
        dirMoveTowards: function (_start, _end) {
            var start = _start.clone();
            start.normalize();
            var end = _end.clone();
            end.normalize();
            var nAngle = end.angleTo(start);
            if (nAngle < this.MAX_ROT_ANGLE) {
                return end;
            }
            var rot = this.MAX_ROT_ANGLE;

            var delta = end.clone();
            delta.sub(start);//方向 start 到 end
            var ll = 0, rr = delta.length(), mid;
            while (ll <= rr) {
                mid = (ll + rr) / 2;
                delta.setLength(mid);
                var ee = delta.clone();
                ee.add(start);//选点
                var nRot = start.angleTo(ee);

                if (Math.abs(nRot - rot) < 0.0001) break;
                if (nRot > rot) {
                    rr = mid;
                } else {
                    ll = mid;
                }
            }
            ee.normalize();
            //    ee.applyAxisAngle(new THREE.Vector3(1,0,0),Math.PI/2);
            return ee;
        }
    }

}

UController.prototype.initEquipmentTree= function () {
    var self = this;
    var ct_setting = {
        callback: {
        }
    };
    $.get(Constants.resourcesURL["equipment_tree"], function (data, status) {
        console.log(data);
        $.fn.zTree.init($("#equipment_tree_inner"), ct_setting, data);
        self.equipments=new Equipments(data);
        self.objects.bimObj.loadBoundingSpheres(function(b){
            self.equipments.loadBoundingSpheres(b, self.view);
        })
        data=null;
    });
}

UController.prototype.initConstructTree = function () {
    var self = this;
    var ct_setting = {
        check: {
            enable: true
        },
        callback: {
            onCheck: function (event, treeId, treeNode) {
                self.lstSelectedIfcId = null;
                self.showOrHideMesh(treeNode, self);
            }
        }
    };
    $.get(Constants.resourcesURL["construct_tree"], function (data, status) {
        $.fn.zTree.init($("#construct_tree_inner"), ct_setting, data);
    });
}

UController.prototype.showOrHideMesh = function (treeNode, self) {
    if (treeNode.children == undefined) {//叶子节点
        if (treeNode.checked) {//显示
            self.objects.bimObj.showOrHideMesh(treeNode.fileId, treeNode.ifcIds, true, self.view.scene);
        } else {//隐藏
            self.objects.bimObj.showOrHideMesh(treeNode.fileId, treeNode.ifcIds, false, self.view.scene);
        }
        return;
    }
    var checked = treeNode.cheched;
    for (var i = 0, len = treeNode.children.length; i < len; i++) {
        self.showOrHideMesh(treeNode.children[i], self);
    }
}

UController.prototype.loadOctree = function () {
    var self = this;
    this.objects.bimObj.loadOctree(function (root) {
        //设置相机
        console.log(root);
        self.view.controls.target = root.boundingBoxMesh.position;
        var radius = root.boundingBoxMesh.geometry.boundingSphere.radius;
        self.view.camera.position.set(self.view.controls.target.x + 2 * radius, self.view.controls.target.y + radius * 2,
            self.view.controls.target.z + radius);
        self.view.controls.update();

        //设置光源
        self.view.dirLight1.position.set(root.maxX * 10, root.maxY * 10, root.maxZ * 10);
    });
}

UController.prototype.addMouseEvent = function () {
    this.addBimViewMouseEvent();
}

UController.prototype.addBimViewMouseEvent = function () {
    var self = this;
    var mouseMoved = false;
    var mouse = {x: 0, y: 0}
    var container = document.getElementById("bim_view");
    container.addEventListener("mousedown", function (e) {
        mouseMoved = false;
    });
    this.view.controls.addEventListener("change", function () {
        mouseMoved = true;
        if(self.equipments!=null)
        self.equipments.updateDomPosition(self.view.camera, self.view.cWidth, self.view.cHeight);
    });
    container.addEventListener("mousemove", function (e) {
        mouse.x = e.clientX / self.view.cWidth * 2 - 1;
        mouse.y = e.clientY / self.view.cHeight * -2 + 1;
    });
    container.addEventListener("mouseup", function (e) {
        if (e.which != 1) return;//监听左键
        if (!mouseMoved) {

            switch (self.mode.code) {
                case self.mode.CODE_NORMAL:
                    self.bimViewClicked_normal(mouse);
                    break;
                case self.mode.CODE_INSP_SLCT:
                    self.bimViewClicked_inspSlct(mouse);
                    break;
            }
        }
    });
}

UController.prototype.bimViewClicked_normal = function (mouse) {//左键点击bim模型界面
    var intersect = this.bimViewClickGetIntersect(mouse);

    if (intersect == null) {

        if (this.lstSelectedIfcId != null)
            this.objects.bimObj.highlightMesh(this.lstSelectedIfcId);//还原纹理
        this.lstSelectedIfcId = null;
        this.oDoms.update_propertyView(null);//更新属性框
        return;
    }


    var ifcId=intersect.face.ifcId;

    this.equipments.userClick(ifcId);

    if (ifcId === this.lstSelectedIfcId) {
        this.objects.bimObj.highlightMesh(this.lstSelectedIfcId);//还原纹理
        this.lstSelectedIfcId = null;
        this.oDoms.update_propertyView(null);//更新属性框
    } else {//选择不同的构件
        if (this.lstSelectedIfcId != null)//取消之前选择的
            this.objects.bimObj.highlightMesh(this.lstSelectedIfcId);
        this.objects.bimObj.highlightMesh(ifcId);
        this.lstSelectedIfcId = ifcId;
        this.oDoms.update_propertyView(ifcId);//更新属性框
    }
};

UController.prototype.bimViewClicked_inspSlct = function (mouse) {
    var intersect = this.bimViewClickGetIntersect(mouse);
    if (intersect == null) return;
    var p = intersect.point;
    this.inspection.addPoint(p);
}

UController.prototype.bimViewClickGetIntersect = function (mouse) {
    return this.objects.bimObj.intersect(mouse,this.view.camera);
};
