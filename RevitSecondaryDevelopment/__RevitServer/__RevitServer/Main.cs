﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.UI.Events;
using System.IO;
using StackExchange.Redis;
using System.Threading;

namespace __RevitServer
{
    [Autodesk.Revit.Attributes.Transaction(TransactionMode.Manual)]
    public class Main : IExternalCommand
    {
        IDatabase db;
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {//初始化


            UIApplication uiApp = commandData.Application;

            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("127.0.0.1");
            db = redis.GetDatabase();

            uiApp.Idling += new EventHandler<IdlingEventArgs>(IdlingHandler);

            return Result.Succeeded;
        }

        public const string DB_FILE_PATH = "Revit:ConvertToIfc";
        public const string DB_STATE = "Revit:State";
        public void IdlingHandler(object sender, IdlingEventArgs args)
        {
            writeTestData(DateTime.Now.Second.ToString());
            return;

            UIApplication uiApp = sender as UIApplication;
            Application app = uiApp.Application;

            string state = db.StringGet(DB_STATE);
            if (state == null) return;
            if (state.Equals("1"))//有任务
            {
                setValue(DB_STATE, "2");//转换中
                String fp = db.StringGet(DB_FILE_PATH);
                writeTestData(fp);
                writeTestData(app.Username);

                string src = fp;
                Document doc = app.OpenDocumentFile(src);
                writeTestData("after open");

                Transaction tr = new Transaction(doc);
                tr.Start("Name1");

                writeTestData("before options");

                IFCExportOptions options = new IFCExportOptions()
                {
                    FileVersion = IFCVersion.IFC2x3
                };

                writeTestData("after options");

                int li = src.LastIndexOf('\\');
                string __fp = src.Substring(0, li);
                string fullName = src.Substring(li + 1, src.Length - li - 1);
                string fn = fullName.Split('.')[0];

                doc.Export(__fp, fn + ".ifc", options);
                tr.Commit();

                doc.Close();

                //    exportToIfc(fp, app);
                setValue(DB_STATE, "3");//转换完成
            }
        }
        private void setValue(string key, string value)
        {
            KeyValuePair<RedisKey, RedisValue> pair = new KeyValuePair<RedisKey, RedisValue>(key, value);
            KeyValuePair<RedisKey, RedisValue>[] pairs = new KeyValuePair<RedisKey, RedisValue>[1];
            pairs[0] = pair;
            db.StringSet(pairs);
        }
        private static void exportToIfc(String src, Application app)
        {
            writeTestData("before open");
            Document doc = app.OpenDocumentFile(src);
            writeTestData("after open");

            Transaction tr = new Transaction(doc);
            tr.Start("Name1");

            writeTestData("before options");

            IFCExportOptions options = new IFCExportOptions()
            {
                FileVersion = IFCVersion.IFC2x3
            };

            writeTestData("after options");

            int li = src.LastIndexOf('\\');
            string fp = src.Substring(0, li);
            string fullName = src.Substring(li + 1, src.Length - li - 1);
            string fn = fullName.Split('.')[0];

            doc.Export(fp, fn + ".ifc", options);
            tr.Commit();

            doc.Close();
        }

        private static void writeTestData(string d)
        {
            FileStream fs = new FileStream(@"F:\1.txt", FileMode.Append);
            byte[] b = System.Text.Encoding.UTF8.GetBytes(d);
            fs.Write(b, 0, b.Length);
        }

    }
}
