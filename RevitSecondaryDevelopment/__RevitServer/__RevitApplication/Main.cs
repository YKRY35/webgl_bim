﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.UI.Events;
using Autodesk.Revit.DB.Events;
using StackExchange.Redis;

namespace __RevitApplication
{
    [Autodesk.Revit.Attributes.Transaction(TransactionMode.Manual)]
    public class Main : IExternalApplication
    {
        IDatabase db;

        public const string DB_FILE_PATH = "Revit:ConvertToIfc";
        public const string DB_STATE = "Revit:State";
        

        public Result OnStartup(UIControlledApplication application)
        {
            application.ControlledApplication.ApplicationInitialized += OnApplicationInitialized;
            return Result.Succeeded;
        }

        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }

        private void OnApplicationInitialized(object sender, ApplicationInitializedEventArgs e)
        {
            //获取UIApplication对象
            Application app = sender as Application;
            UIApplication uiApp = new UIApplication(app);

            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("127.0.0.1");
            db = redis.GetDatabase();

            uiApp.Idling += HandleMessage;
        }
        int cnt = 0;
        //循环处理消息
        private void HandleMessage(object sender, IdlingEventArgs e)
        {
            try
            {
                string state = db.StringGet(DB_STATE);
                if (state == null) return;
                if (state.Equals("1"))//有任务
                {
                    setValue(DB_STATE, "2");//转换中
                    String fp = db.StringGet(DB_FILE_PATH);

                    UIApplication uiApp = sender as UIApplication;
                    Application app = uiApp.Application;

                    exportToIfc(fp, app);

                    setValue(DB_STATE, "3");//转换完成
                }
            }
            catch (Exception ex)
            {
                TaskDialog.Show("error", ex.ToString());
            }
        }
        private void setValue(string key, string value)
        {
            KeyValuePair<RedisKey, RedisValue> pair = new KeyValuePair<RedisKey, RedisValue>(key, value);
            KeyValuePair<RedisKey, RedisValue>[] pairs = new KeyValuePair<RedisKey, RedisValue>[1];
            pairs[0] = pair;
            db.StringSet(pairs);
        }
        private void exportToIfc(String src,Application app)
        {
            Document doc = app.OpenDocumentFile(src);

            Transaction tr = new Transaction(doc);
            tr.Start("Export");

            IFCExportOptions options = new IFCExportOptions()
            {
                FileVersion = IFCVersion.IFC2x3
            };

            char fchar = src.Contains("/") ? '/' : '\\';
            int li = src.LastIndexOf(fchar);
            string fp = src.Substring(0, li);
            string fullName = src.Substring(li + 1, src.Length - li - 1);
            string fn = fullName.Split('.')[0];

            doc.Export(fp, fn + ".ifc", options);
            tr.Commit();

            doc.Close();
        }
    }
}
