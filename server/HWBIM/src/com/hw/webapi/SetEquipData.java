package com.hw.webapi;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hw.utils.DBUtil;

public class SetEquipData extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("receive");
		
		String alias = req.getParameter("alias");
		String id = req.getParameter("id");
		String data=req.getParameter("data");
		if (alias != null&&id!=null) {
			System.out.println(data);
			DBUtil.setEquipData(alias, id, data);
		}

	}

}