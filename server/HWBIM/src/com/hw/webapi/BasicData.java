package com.hw.webapi;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hw.utils.DBUtil;

import net.sf.json.JSONObject;

public class BasicData extends HttpServlet {

	@Override
	public void init() throws ServletException {
		super.init();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String alias = req.getParameter("alias");
		if (alias == null)
			return;
		JSONObject obj = new JSONObject();
		obj.put("TriangleAmount", DBUtil.getTriangleAmount(alias));
		resp.getWriter().write(obj.toString());
		
	}

}