package com.hw.webapi;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hw.outside.FileInfo;
import com.hw.utils.DBUtil;

import net.sf.json.JSONArray;

public class FileList extends HttpServlet {

	@Override
	public void init() throws ServletException {
		super.init();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setContentType("text/json");
		resp.setCharacterEncoding("utf-8");

		PrintWriter pw = resp.getWriter();
		
		String[] aliases = DBUtil.getFileList();// ����
		
		int lLen = aliases.length;
		
		
		JSONArray arr=new JSONArray();
		for (int i = 0; i < lLen; i++) {
			arr.add(new FileInfo(aliases[i], DBUtil.aliasToSrcName(aliases[i]), DBUtil.getAliasState(aliases[i])));
		}

		pw.write(arr.toString());

	}

}
