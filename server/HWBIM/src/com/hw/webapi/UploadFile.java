package com.hw.webapi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.hw.outside.ConversionThreadPool;
import com.hw.utils.DBUtil;

public class UploadFile extends HttpServlet {// 文件上传

	private final String tmpFilePath = "F:/PROJECTS/PROGRAMMING/Web/nginx_bim/tmp_file";// 临时文件目录
	private final String saveFilePath1 = "F:/PROJECTS/PROGRAMMING/Web/nginx_bim/data/resources/HWBIM/";// 文件存储目录最长公共前缀
	private final String saveFileSrcFileFolderName = "SrcFile";// 源文件文件夹名称

	String alias;
	
    // 上传配置
    private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
    private static final int MAX_FILE_SIZE      = 1024 * 1024 * 200; // 200MB
    private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
	

	private boolean saveFile(HttpServletRequest request) throws IOException{
		 // 配置上传参数
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // 设置内存临界值 - 超过后将产生临时文件并存储于临时目录中
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        // 设置临时存储目录
        factory.setRepository(new File("F:/PROJECTS/PROGRAMMING/Web/nginx_bim/tmp_file"));
        
		ServletFileUpload upload = new ServletFileUpload(factory);
		 
		// 设置最大文件上传值
        upload.setFileSizeMax(MAX_FILE_SIZE);
         
        // 设置最大请求值 (包含文件和表单数据)
        upload.setSizeMax(MAX_REQUEST_SIZE);

        // 中文处理
        upload.setHeaderEncoding("UTF-8"); 

        // 构造临时路径来存储上传的文件
        // 这个路径相对当前应用的目录
        String uploadPath;
 
        try {
            // 解析请求的内容提取文件数据
            @SuppressWarnings("unchecked")
            List<FileItem> formItems = upload.parseRequest(request);
 
            if (formItems != null && formItems.size() > 0) {
                // 迭代表单数据
                for (FileItem item : formItems) {
                    // 处理不在表单中的字段
                    if (!item.isFormField()) {
                        String fileName = new File(item.getName()).getName();
                        
                        if(fileName.equals("")){
                        	return false;
                        }
                        
                        alias=DBUtil.newFile(fileName);
                        File uploadPathFile=new File(saveFilePath1+alias,saveFileSrcFileFolderName);
                        if(!uploadPathFile.exists()) uploadPathFile.mkdirs();
                        uploadPath=uploadPathFile.getAbsolutePath();
                        
                        String filePath = uploadPath + File.separator + fileName;
                        File storeFile = new File(filePath);
                        // 在控制台输出文件的上传路径
                        System.out.println(filePath);
                        // 保存文件到硬盘
                        item.write(storeFile);
                    }
                }
            }
        }catch (Exception ex) {
            request.setAttribute("message",
                    "错误信息: " + ex.getMessage());
            return false;
        }
        return true;
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		PrintWriter pw=resp.getWriter();
		
		pw.write("<script language=\"javascript\">");
		if(saveFile(req)){
			
			pw.write("alert(\"success\");\n");

			// 丢入线程池，进行转换
			DBUtil.setAliasState(alias, DBUtil.ALIAS_CONVERTING);
			ConversionThreadPool.getInstance().addTask(new com.hw.outside.ConversionTask(alias, null));

		}else{//保存失败
			pw.write("alert(\"fail\");\n");
		}
		pw.write("self.location=\"http://localhost:35000/HWBIM/cons/index.html\";");
		pw.write("</script>");

	}

	public String saveFile(File tmpFile) throws IOException {// 保存文件，更新数据库
		RandomAccessFile randomFile = new RandomAccessFile(tmpFile, "r");

		randomFile.readLine();
		String fileName = new String(randomFile.readLine().getBytes("8859_1"), "utf-8");
		fileName = fileName.substring(fileName.lastIndexOf('=') + 2, fileName.length() - 1);
		fileName = new File(fileName).getName();
		System.out.println(fileName);// 文件名

		if (fileName.equals(""))
			return null;

		int cnt = 0, n;

		long startPosition = 0;
		randomFile.seek(0);
		while ((n = randomFile.readByte()) != -1 && cnt < 4) {
			if (n == '\n') {
				cnt++;
			}
		}
		startPosition = randomFile.getFilePointer() - 1;// 第四个\n后一位

		randomFile.seek(randomFile.length());
		long endPosition = randomFile.getFilePointer();
		cnt = 0;
		byte lstByte = 0;
		while (endPosition > 0 && cnt < 6) {
			endPosition--;
			randomFile.seek(endPosition);
			byte nByte = randomFile.readByte();
			if (nByte == 0x0d && lstByte == 0x0a)// 0d0a
				cnt++;
			lstByte = nByte;
		}
		// endPosition--;// 倒数第六个\r\a前一位

		// endPosition=randomFile.length();

		randomFile.close();

		// startPosition endPosition 为有效字段

		String alias = DBUtil.newFile(fileName);// 保存到数据库中 获得文件别名

		File savePath1 = new File(saveFilePath1, alias);
		if (!savePath1.exists())
			savePath1.mkdirs();

		File savePath2 = new File(savePath1, saveFileSrcFileFolderName);
		if (!savePath2.exists())
			savePath2.mkdirs();

		File saveFile = new File(savePath2, fileName);

		byte[] b = new byte[1024 * 1024 * 30];// 30MB
		InputStream is = new FileInputStream(tmpFile);
		OutputStream os = new FileOutputStream(saveFile);

		long nLeft = 0, nRight;// 在原数据中的区间
		while ((n = is.read(b)) != -1) {
			nRight = nLeft + n;
			if (nRight > startPosition && nLeft < endPosition) {
				int off = 0;
				if (nLeft < startPosition)
					off = (int) (startPosition - nLeft);
				int len = (int) (Math.min(endPosition, nRight) - Math.max(startPosition, nLeft));
				os.write(b, off, len);
			}
			nLeft += n;
		}

		is.close();
		os.close();

		tmpFile.delete();// 刪除临时文件

		return alias;
	}

}