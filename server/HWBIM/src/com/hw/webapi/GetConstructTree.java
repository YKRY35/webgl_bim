package com.hw.webapi;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hw.utils.DBUtil;

public class GetConstructTree extends HttpServlet {

	@Override
	public void init() throws ServletException {
		super.init();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setContentType("text/json");
		resp.setCharacterEncoding("utf-8");

		String alias = req.getParameter("alias");
		if (alias != null) {
			String tree = DBUtil.getConstructTree(alias);

			// System.out.println(tree);
			if (tree != null)
				resp.getWriter().write(tree);
		}

	}

}