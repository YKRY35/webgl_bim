package com.hw.webapi;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hw.utils.DBUtil;

public class ElemAttr extends HttpServlet {

	private final String KEY_ALIAS = "alias";
	private final String KEY_IFC_ID = "IfcId";

	@Override
	public void init() throws ServletException {
		super.init();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		String alias = req.getParameter(KEY_ALIAS);
		String ifcId = req.getParameter(KEY_IFC_ID);
		
		resp.setContentType("text/json");
		resp.setCharacterEncoding("utf-8");
		PrintWriter pw = resp.getWriter();
			
		pw.write(DBUtil.getIfcAttr(alias, ifcId));
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

}