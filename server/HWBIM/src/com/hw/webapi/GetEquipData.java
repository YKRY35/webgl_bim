package com.hw.webapi;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hw.utils.DBUtil;

public class GetEquipData extends HttpServlet {


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setContentType("text/json");
		resp.setCharacterEncoding("utf-8");

		String alias = req.getParameter("alias");
		String id = req.getParameter("id");
		if (alias != null&&id!=null) {
			
			String data = DBUtil.getEquipData(alias, id);

			// System.out.println(tree);
			if (data != null)
				resp.getWriter().write(data);
			else resp.getWriter().write("{}");
		}
	}

}