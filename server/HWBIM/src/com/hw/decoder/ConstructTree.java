package com.hw.decoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ConstructTree {// 别忘了在解析完obj后更新meshIds

	Map<String, ConstructTree> subTrees;// 子树

	public static final int MAX_DEPTH = 4;

	// 叶子节点拥有的属性
	private int fileId;
	private List<Integer> ifcIds;

	public ConstructTree() {
		subTrees = new HashMap<String, ConstructTree>();
	}

	public ConstructTree add(String subName) {
		if (subName == null)
			subName = "未命名";
		if (subTrees.containsKey(subName)) {
			return subTrees.get(subName);
		} else {
			ConstructTree _subTree = new ConstructTree();
			this.subTrees.put(subName, _subTree);
			return _subTree;
		}
	}

	public void addIfcId(int ifcId) {
		if (ifcIds == null)
			ifcIds = new ArrayList<Integer>();
		ifcIds.add(ifcId);
		
	}

	public String toJSON() {

		return _toJSON(this, "root", 0);
	}

	private static String _toJSON(ConstructTree node, String name, int depth) {
		JSONObject obj = new JSONObject();
		obj.put("name", name);
		if (depth == 0) {// 根节点 自动打开
			obj.put("open", "true");
		}
		if (depth == MAX_DEPTH) {
			JSONArray jIfcIds = new JSONArray();
			for (int i = 0, l = node.ifcIds.size(); i < l; i++)
				jIfcIds.add(node.ifcIds.get(i));
			obj.put("ifcIds", jIfcIds.toString());
			obj.put("fileId", node.fileId);
			return obj.toString();
		}
		JSONArray array = new JSONArray();

		for (String subTreeName : node.subTrees.keySet()) {
			array.add(_toJSON(node.subTrees.get(subTreeName), subTreeName, depth + 1));
		}
		obj.put("children", array.toString());
		return obj.toString();
	}

	private int __fileId;
	private Map<Integer, Integer> map_ifcId2FileId;
	public Map<Integer, Integer> updateFileId() {// 更新 familyType Id，记为fileId
		__fileId = 0;
		map_ifcId2FileId=new HashMap<Integer, Integer>();
		this._updateFileId(this, 0);
		return map_ifcId2FileId;
	}

	private void _updateFileId(ConstructTree node, int depth) {
		if (depth == MAX_DEPTH) {// 叶节点
			node.fileId = __fileId;
			__fileId++;
			for(int ifcId: node.ifcIds)
				map_ifcId2FileId.put(ifcId, node.fileId);
			return;
		}
		for (String subTreeName : node.subTrees.keySet()) {
			_updateFileId(node.subTrees.get(subTreeName), depth + 1);
		}
	}
}