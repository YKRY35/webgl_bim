package com.hw.decoder;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import ifc2x3javatoolbox.ifc2x3tc1.DOUBLE;
import ifc2x3javatoolbox.ifc2x3tc1.IfcAxis2Placement;
import ifc2x3javatoolbox.ifc2x3tc1.IfcAxis2Placement3D;
import ifc2x3javatoolbox.ifc2x3tc1.IfcBuildingElementProxy;
import ifc2x3javatoolbox.ifc2x3tc1.IfcCartesianPoint;
import ifc2x3javatoolbox.ifc2x3tc1.IfcDirection;
import ifc2x3javatoolbox.ifc2x3tc1.IfcElement;
import ifc2x3javatoolbox.ifc2x3tc1.IfcLabel;
import ifc2x3javatoolbox.ifc2x3tc1.IfcLengthMeasure;
import ifc2x3javatoolbox.ifc2x3tc1.IfcLocalPlacement;
import ifc2x3javatoolbox.ifc2x3tc1.IfcObjectPlacement;
import ifc2x3javatoolbox.ifc2x3tc1.IfcPresentationLayerAssignment;
import ifc2x3javatoolbox.ifc2x3tc1.IfcProductRepresentation;
import ifc2x3javatoolbox.ifc2x3tc1.IfcProperty;
import ifc2x3javatoolbox.ifc2x3tc1.IfcPropertySet;
import ifc2x3javatoolbox.ifc2x3tc1.IfcPropertySetDefinition;
import ifc2x3javatoolbox.ifc2x3tc1.IfcPropertySingleValue;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelContainedInSpatialStructure;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelDefines;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelDefinesByProperties;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRelDefinesByType;
import ifc2x3javatoolbox.ifc2x3tc1.IfcRepresentation;
import ifc2x3javatoolbox.ifc2x3tc1.IfcSpatialStructureElement;
import ifc2x3javatoolbox.ifc2x3tc1.IfcTypeObject;
import ifc2x3javatoolbox.ifc2x3tc1.LIST;
import ifc2x3javatoolbox.ifc2x3tc1.SET;
import ifc2x3javatoolbox.ifcmodel.IfcModel;
import net.sf.json.JSONObject;

public class IfcDecoder {
	private IfcModel ifcModel;

	private IfcModelAttr modelAttr;
	private JSONObject[] elemAttr;// 属性 下标就是ifcId
	private ConstructTree constructTree;
	private EquipmentListInMap equipLinM;

	//private List<List<Integer>> familyTypes;
	private Map<Integer, Integer> map_ifcId2FileId;

	
	private String _currFamilyType;
	
	private MyEquipments equipments;
	

	public IfcDecoder() {// 构造函数
		equipments=MyEquipments.getInstance();
	}

	public void readAndDecode(Object lock, String filePath, int meshAmount, Map<String, List<Integer>> map_ifcName2Id) {// 解析
		synchronized(lock){//锁
			ifcModel = new IfcModel();
			try {
				ifcModel.readStepFile(new File(filePath));
			} catch (Exception e) {
				e.printStackTrace();
				return ;
			}
			if (ifcModel == null)
				return;
			Collection<IfcElement> elements = ifcModel.getCollection(IfcElement.class);
			modelAttr = new IfcModelAttr();
			modelAttr.elemAmount = meshAmount;

			elemAttr = new JSONObject[modelAttr.elemAmount];

			constructTree = new ConstructTree();// 根
			equipLinM = new EquipmentListInMap();

			for (IfcElement elem : elements) {
				int ifcId;
				String transName = this.translateIfcName(this.getElemSrcName(elem));
				if (!map_ifcName2Id.containsKey(transName)) {// 并非拥有几何特性的构件
					continue;
				}
				if (map_ifcName2Id.get(transName).size() == 0)
					continue;
				ifcId = map_ifcName2Id.get(transName).remove(0);
				decodeIfcElement(elem, ifcId);
			}

			map_ifcId2FileId = constructTree.updateFileId();
		}
	}

	private String getElemSrcName(IfcElement elem) {// element名字到id的映射
		// name转换
		IfcLabel label = elem.getName();
		String name = label.getDecodedValue();
		return name;
	}

	private String translateIfcName(String name) {// 汉字变成 _ (下划线)
		int nameLen = name.length();
		StringBuffer transBuffer = new StringBuffer(nameLen);
		for (int i = 0; i < nameLen; i++) {
			int _c = name.charAt(i);
			if (_c == ' ' || _c > 127 || _c < 0) {// 空格 汉字 替换成下划线
				transBuffer.append('_');
			} else {
				transBuffer.append((char) _c);
			}
		}
		String transName = transBuffer.toString();
		return transName;
	}

	private void decodeIfcElement(IfcElement elem, int ifcId) {
		elemAttr[ifcId] = new JSONObject();
		decodeBaseInf(elem, ifcId);
		decodePlacement(elem, ifcId);
		decodeDefinedBy(elem, ifcId);
		decodeContainedIn(elem, ifcId);
		decodeCategory(elem, ifcId);

		updateConstructTree(ifcId);
		updateEquipmentLinM(elem, ifcId);
	}

	private void decodeBaseInf(IfcElement elem, int ifcId) {
		// globalId
		elemAttr[ifcId].put("globalId", elem.getGlobalId().toString());

		// name
		elemAttr[ifcId].put("name", this.getElemSrcName(elem));

		// object type
		elemAttr[ifcId].put("objectType", elem.getObjectType().toString());
		// System.out.println(elem.getObjectType());

		// tag
		elemAttr[ifcId].put("tag", elem.getTag() == null ? null : elem.getTag().toString());
	}

	private void decodePlacement(IfcElement elem, int ifcId) {
		IfcObjectPlacement placement = elem.getObjectPlacement();
		if (placement.getClass() == IfcLocalPlacement.class) {
			IfcLocalPlacement localPlacement = (IfcLocalPlacement) placement;
			IfcAxis2Placement axis2Placement = localPlacement.getRelativePlacement();
			if (axis2Placement.getClass() == IfcAxis2Placement3D.class) {
				IfcAxis2Placement3D axis2Placement3D = (IfcAxis2Placement3D) axis2Placement;

				// axis
				IfcDirection axis = axis2Placement3D.getAxis();
				elemAttr[ifcId].put("axis", ifcDirectionDecode(axis));
				// System.out.println(ifcDirectionDecode(axis));

				// direction
				IfcDirection direction = axis2Placement3D.getRefDirection();
				elemAttr[ifcId].put("direction", ifcDirectionDecode(direction));

				// System.out.println(ifcDirectionDecode(direction));

				// location
				IfcCartesianPoint location = axis2Placement3D.getLocation();
				elemAttr[ifcId].put("location", ifcCartesianPointDecode(location));
				// System.out.println(ifcCartesianPointDecode(location));

			}
		}
	}

	private void decodeDefinedBy(IfcElement elem, int ifcId) {
		SET<IfcRelDefines> relDefineses = elem.getIsDefinedBy_Inverse();
		if (relDefineses == null)
			return;
		for (IfcRelDefines defines : relDefineses) {
			if (defines instanceof IfcRelDefinesByType) {
				IfcRelDefinesByType type = (IfcRelDefinesByType) defines;
				IfcTypeObject relatingType = type.getRelatingType();

				// family
				elemAttr[ifcId].put("family", ifcClassDecode(relatingType.getClass().toString()));
				// System.out.println(ifcClassDecode(relatingType.getClass().toString()));

				// familyType
				this._currFamilyType=relatingType.getName().toString();
				elemAttr[ifcId].put("familyType", this._currFamilyType);

				// System.out.println(relatingType.getName());

				// IfcWallType t=(IfcWallType) relatingType;
				// System.out.println(t);
				// System.out.println(t.getPredefinedType());
			} else if (defines instanceof IfcRelDefinesByProperties) {
				IfcRelDefinesByProperties _defines = (IfcRelDefinesByProperties) defines;
				IfcPropertySetDefinition pSet = _defines.getRelatingPropertyDefinition();
				if (pSet instanceof IfcPropertySet) {
					IfcPropertySet propertySet = (IfcPropertySet) pSet;
					SET<IfcProperty> properties = propertySet.getHasProperties();
					for (IfcProperty property : properties) {
						if (property instanceof IfcPropertySingleValue) {
							IfcPropertySingleValue singleValue = (IfcPropertySingleValue) property;

							// 额外属性
							elemAttr[ifcId].put(singleValue.getName(), singleValue.getNominalValue().toString());
						}
					}
				}
			}
		}
	}

	private void decodeContainedIn(IfcElement elem, int ifcId) {
		SET<IfcRelContainedInSpatialStructure> structures = elem.getContainedInStructure_Inverse();

		if (structures != null)
			for (IfcRelContainedInSpatialStructure structure : structures) {
				IfcSpatialStructureElement spatialStructure = structure.getRelatingStructure();

				// floor
				elemAttr[ifcId].put("floor", spatialStructure.getName().toString());
				// System.out.println(spatialStructure.getName());
			}

	}

	private void decodeCategory(IfcElement elem, int ifcId) {
		IfcProductRepresentation productRepresentation = elem.getRepresentation();
		if (productRepresentation == null)
			return;
		LIST<IfcRepresentation> representations = productRepresentation.getRepresentations();
		if (representations == null)
			return;
		for (IfcRepresentation r : representations) {
			SET<IfcPresentationLayerAssignment> assignments = (r.getLayerAssignments_Inverse());
			if (assignments != null) {
				for (IfcPresentationLayerAssignment assignment : assignments) {

					// category name
					elemAttr[ifcId].put("categoryName", assignment.getName().toString());
					// System.out.println(assignment.getName());

					// System.out.println(assignment.getStepLineNumber());

					break;
				}
				break;
			}
		}

	}

	private String ifcDirectionDecode(IfcDirection dir) {
		if (dir == null)
			return null;
		LIST<DOUBLE> ratios = dir.getDirectionRatios();
		StringBuffer tmp = new StringBuffer();
		boolean first = true;
		for (DOUBLE d : ratios) {
			if (!first)
				tmp.append(" ");
			else
				first = false;
			tmp.append(d);
		}
		return tmp.toString();
	}

	private String ifcCartesianPointDecode(IfcCartesianPoint point) {
		if (point == null)
			return null;
		LIST<IfcLengthMeasure> measure = point.getCoordinates();
		StringBuffer tmp = new StringBuffer();
		boolean first = true;
		for (IfcLengthMeasure m : measure) {
			if (!first)
				tmp.append(" ");
			else
				first = false;

			tmp.append(m);
		}
		return tmp.toString();
	}

	private String ifcClassDecode(String src) {
		String[] tmp = src.split("\\.");
		return tmp[tmp.length - 1];
	}

	private void updateConstructTree(int ifcId) {
		String level[] = new String[] { (String) elemAttr[ifcId].get("floor"),
				(String) elemAttr[ifcId].get("categoryName"), (String) elemAttr[ifcId].get("family"),
				(String) elemAttr[ifcId].get("familyType") };
		ConstructTree node = constructTree;// 操作节点
		for (int i = 0; i < ConstructTree.MAX_DEPTH; i++) {// level.length() = 5
			node = node.add(level[i]);
		}
		// 此时node为叶节点 表示familyType
		// 填入ifcId
		node.addIfcId(ifcId);
	}

	private void updateEquipmentLinM(IfcElement elem, int ifcId) {
	//	if (elem instanceof IfcBuildingElementProxy) {
			String equipName=equipments.checkExistence(this.getElemSrcName(elem));
			if(equipName!=null)//是设备
				this.equipLinM.addEquipment(equipName, elem.getName().toString(),ifcId);
	//	}
	}

	public JSONObject[] getElemAttr() {
		return this.elemAttr;
	}

	public ConstructTree getConstructTree() {
		return this.constructTree;
	}

	public Map<Integer, Integer> get_familyTypes() {
		return this.map_ifcId2FileId;
	}
	
	public EquipmentListInMap get_quipments(){
		return this.equipLinM;
	}
}

class IfcModelAttr {
	int elemAmount;
}
