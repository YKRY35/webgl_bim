package com.hw.decoder;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.hw.mesh.BoundingBox;
import com.hw.mesh.BoundingSphere;
import com.hw.mesh.MeshGroup;
import com.hw.mesh.MtlContent;
import com.hw.mesh.MultiMesh;
import com.hw.mesh.Octree;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ThreeDDecoder {

	private List<MtlContent> mtls;
	private Map<String, Integer> map_mtl2Id;// mtl name到id的映射
	List<MultiMesh> meshes;

	private Map<String, List<Integer>> map_ifcName2Id;
	private int meshAmount;

	BoundingBox boundingBox;// 整个的包围盒

//	public int triangleNum = 0;// 三角形数量

	Octree octree;
	
	private List<BoundingSphere> bSpheres;

	public ThreeDDecoder() {
		mtls = new ArrayList<MtlContent>();
		map_mtl2Id = new HashMap<String, Integer>();
		meshes = new ArrayList<MultiMesh>();
		map_ifcName2Id = new HashMap<String, List<Integer>>();
		bSpheres=new ArrayList<BoundingSphere>();
	}

	public List<MtlContent> getMtls() {
		return this.mtls;
	}

	public List<MultiMesh> getMeshes() {
		return this.meshes;
	}

	public Octree getOctree() {
		return this.octree;
	}
	
	public String getBoundingSpheres(){
		JSONObject arr=new JSONObject();
		for(int i=0;i<bSpheres.size();++i){
			JSONObject obj=new JSONObject();
			
			BoundingSphere bs=bSpheres.get(i);
			obj.put("centerX", bs.centerX);
			obj.put("centerY", bs.centerY);
			obj.put("centerZ", bs.centerZ);
			obj.put("radius", bs.radius);
			
			arr.put(i, obj.toString());
		}
		return arr.toString();
	}

	public void decode(String objPath, String mtlPath) throws IOException {
		BufferedReader br;
		InputStreamReader isr;
		FileInputStream fis;

		fis = new FileInputStream(mtlPath);
		isr = new InputStreamReader(fis);
		br = new BufferedReader(isr);

		decodeMTL(br);// 解析mtl

		br.close();// 上面能够解析说明br非空
		isr.close();
		fis.close();

		fis = new FileInputStream(objPath);
		isr = new InputStreamReader(fis);
		br = new BufferedReader(isr);

		decodeObj(br);// 解析obj

		br.close();// 上面能够解析说明br非空
		isr.close();
		fis.close();

		calcOctree();
		calcBoundingSpheres();

	}

	private void decodeMTL(BufferedReader br) throws IOException {// 二进制文件
																	// mtl.docx

		mtls.clear();
		map_mtl2Id.clear();
		String line = null;
		MtlContent tmpMtl = null;
		while ((line = br.readLine()) != null) {
			line = line.trim();
			if (line.equals("")) {// 空行
				continue;
			}

			String[] params = line.split(" ");
			if (params[0].equals("newmtl")) {// newmtl
				tmpMtl = new MtlContent();
				map_mtl2Id.put(params[1], mtls.size());// 更新纹理名称到id的映射
				mtls.add(tmpMtl);// 添加新的纹理
			} else if (params[0].equals("Kd")) {// color
				tmpMtl.color = fRGB2Int(params[1], params[2], params[3]);
			} else if (params[0].equals("Ks")) {// specular
				tmpMtl.specular = fRGB2Int(params[1], params[2], params[3]);
			} else if (params[0].equals("Ns")) {// Ns
				tmpMtl.shininess = Integer.parseInt(params[1]);
			} else if (params[0].equals("Tr")) {
				tmpMtl.opacity = Float.parseFloat(params[1]);
			}
		}
	}

	// ifcId 按geometry顺序递增
	private void decodeObj(BufferedReader br) throws IOException {
		int index = -1;
		this.meshAmount = 0;

		meshes.clear();
		String line = null;
		MultiMesh tmpMesh = null;
		MeshGroup tmpMg = null;
		int tmpVAmount = 0;
		boundingBox = new BoundingBox();
		
		long st=System.currentTimeMillis();

		while ((line = br.readLine()) != null) {
			line = line.trim();
			if (line.equals("")) {// 空行
				continue;
			}

			String[] params = line.split(" ");
			if (params[0].equals("g")) {// 新建geometry mesh
				// 添加新的geometry前，记录前一个geometry的vertex数量
				if (tmpMesh != null) {// 不是第一个mesh
					tmpVAmount += tmpMesh.getVertNum();
				}

				index++;

				tmpMesh = new MultiMesh();
				this.updateIfcName2Id(params[1], index);
				bSpheres.add(new BoundingSphere());
				meshAmount++;
				meshes.add(tmpMesh);
			} else if (params[0].equals("v")) {// 新的v点

				tmpMesh.putV(new String[] { params[1], params[2], params[3] });
				float x = Float.parseFloat(params[1]);
				float y = Float.parseFloat(params[2]);
				float z = Float.parseFloat(params[3]);// 没写好，有点蠢
				
				bSpheres.get(index).newVertex(x, y, z);
				this.boundingBox.newVertex(x, y, z);// 更新自身包围盒
			} else if (params[0].equals("vn")) {
			//	tmpMesh.putVN(new String[] { params[1], params[2], params[3] });
			} else if (params[0].equals("f")) {
		//		this.triangleNum++;// 三角形数量

				String[] faces = new String[3];
				for (int j = 1; j < 4; j++) {
					faces[j - 1] = params[j].split("//")[0];
				}
				tmpMg.putF(faces, tmpVAmount);
			} else if (params[0].equals("usemtl")) {// 纹理
				tmpMg = new MeshGroup(getMtlId(params[1]), index);
				tmpMesh.putMeshGroup(tmpMg);
			}
		}

		
		System.out.println("read1 time   "+(System.currentTimeMillis()-st));
	}

	private void updateIfcName2Id(String name, int index) {
		if (this.map_ifcName2Id.containsKey(name)) {
			this.map_ifcName2Id.get(name).add(index);
		} else {
			List<Integer> l = new ArrayList<Integer>();
			l.add(index);
			this.map_ifcName2Id.put(name, l);
		}
	}

	public static int fRGB2Int(String r, String g, String b) {
		float fr = Float.parseFloat(r);
		float fg = Float.parseFloat(g);
		float fb = Float.parseFloat(b);

		int ir = Math.round(fr * 255);
		int ig = Math.round(fg * 255);
		int ib = Math.round(fb * 255);

		return (ir << 16) + (ig << 8) + (ib);
	}

	private int getMtlId(String mtlName) {
		return map_mtl2Id.get(mtlName);
	}

	private void calcOctree() {// 计算八叉树
		octree = new Octree(boundingBox);
		int len = meshes.size();
		for (int i = 0; i < len; i++) {
			octree.addData(meshes.get(i));
		}
		Octree.transform(octree);
	}
	
	private void calcBoundingSpheres(){
		for(int i=0;i<bSpheres.size();i++){
			bSpheres.get(i).calc(meshes.get(i));
		}
	}

	public Map<String, List<Integer>> get_mapIfcName2Id() {
		return this.map_ifcName2Id;
	}

	public int getMeshAmount() {
		return this.meshAmount;
	}
}


