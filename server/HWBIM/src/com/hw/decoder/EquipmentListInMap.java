package com.hw.decoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class EquipmentListInMap {
	private Map<String, List<OneEquipment>> data;

	private int cnt;

	public EquipmentListInMap() {
		data = new HashMap<String, List<OneEquipment>>();
		cnt = 0;
	}

	public void addEquipment(String equipName, String eName,int ifcId) {
		if (!data.containsKey(equipName)) {
			data.put(equipName, new ArrayList<OneEquipment>());
		}
		OneEquipment equ=new OneEquipment(eName, ifcId, cnt++);
		data.get(equipName).add(equ);
	}

	public String toJSON() {
		JSONArray rootArr = new JSONArray();
		for (String equipName : data.keySet()) {
			JSONObject node = new JSONObject();

			node.put("name", equipName);

			List<OneEquipment> es = data.get(equipName);

			JSONArray children = new JSONArray();
			for (OneEquipment equip : es) {
				JSONObject child = new JSONObject();
				child.put("name", equip.equipmentName);
				child.put("ifcId", equip.ifcId);
				child.put("equipId", equip.equipId);

				children.add(child.toString());
			}

			node.put("children", children.toString());

			rootArr.add(node.toString());
		}
		return rootArr.toString();
	}
}

class OneEquipment{
	String equipmentName;
	int ifcId;
	int equipId;
	public OneEquipment(String en,int iid,int eid){
		this.equipmentName=en;
		this.ifcId=iid;
		this.equipId=eid;
	}
}