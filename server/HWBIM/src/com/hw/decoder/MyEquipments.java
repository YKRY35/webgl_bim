package com.hw.decoder;

import java.util.HashMap;
import java.util.Map;

public class MyEquipments{
	private static MyEquipments instance=null;
	public static MyEquipments getInstance(){
		if(instance==null) instance=new MyEquipments();
		return instance;
	}
	
	private Map<String, Boolean> exist;
	private String[] equipNames=new String[]{
			"�����","��ˮ��¯","����̽����1"
	};
	public MyEquipments(){
		exist=new HashMap<String, Boolean>();
		initData();
	}
	
	private void initData(){
		for(int i=0,l=equipNames.length;i<l;i++){
			exist.put(equipNames[i], true);
		}
	}
	
	private final String split_flag[]={
			"-",":"
	};
	public String checkExistence(String name){
		for(String f:split_flag){
			String[] ss=name.split(f);
			if(ss.length>0){
				String tName=ss[0].trim();
				if(exist.containsKey(tName))
					return tName;
			}
		}
		return null;
	}
}