package com.hw.mesh;

import com.hw.decoder.ThreeDDecoder;

public class MtlContent {
	public int color;
	public int specular;
	public int shininess;
	public float opacity;

	public MtlContent() {// ����Ĭ��ֵ
		this.color = ThreeDDecoder.fRGB2Int("0.8", "0.8", "0.8");
		this.specular = ThreeDDecoder.fRGB2Int("1.0", "1.0", "1.0");
		this.shininess = 0;
		this.opacity = 1.0f;
	}
}
