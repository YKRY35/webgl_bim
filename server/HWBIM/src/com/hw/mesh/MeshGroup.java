package com.hw.mesh;

import java.util.ArrayList;
import java.util.List;

public class MeshGroup{
	
	List<Integer> faces;// 此处v和vn的index总是相同
	
	int materialId;
	int ifcId;
	
	public MeshGroup(int materialId, int ifcId){
		faces=new ArrayList<Integer>();
		this.materialId=materialId;
		this.ifcId=ifcId;
	}
	
	public void putF(String[] fs, int deltaV) {// 减去deltaV+1
		for (int i = 0; i < 3; i++) {
			this.faces.add(Integer.parseInt(fs[i]) - deltaV - 1);
		}
	}
}