package com.hw.mesh;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LOD {
	private final int THREAD_POOL_SIZE = 2;
	private final int LOD_LEVELS = 10;

	final Map<Integer, MultiMesh> combinedMeshes;

	Map<Point, Set<PointInfo>> pointsInfo;

	File srcRoot;
	File savingRoot;

	int fileNum;

	int srcFaceNum;

	public LOD(final Map<Integer, MultiMesh> combinedMeshes, File src, File saving) {
		pointsInfo = new HashMap<Point, Set<PointInfo>>();
		this.combinedMeshes = combinedMeshes;
		this.srcRoot = src;
		this.savingRoot = saving;
		init();
		generate();
	}

	private void init() {
		this.fileNum = 0;
		this.srcFaceNum = 0;
		for (int fileId : combinedMeshes.keySet()) {
			this.fileNum++;
			MultiMesh singleMesh = combinedMeshes.get(fileId);
			for (int groupId = 0; groupId < singleMesh.groups.size(); groupId++) {
				MeshGroup mg = singleMesh.groups.get(groupId);
				this.srcFaceNum += mg.faces.size() / 3;
				for (int index : mg.faces) {
					// 点
					float vx = singleMesh.vertices.get(index * 3);
					float vy = singleMesh.vertices.get(index * 3 + 1);
					float vz = singleMesh.vertices.get(index * 3 + 2);

					Point p = new Point(vx, vy, vz);
					if (!pointsInfo.containsKey(p)) {
						pointsInfo.put(p, new HashSet<PointInfo>());
					}
					pointsInfo.get(p).add(new PointInfo(fileId, groupId));

				}
			}
		}
	}

	void generate() {

		// ExecutorService threadPool =
		// Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		try{
			for (int i = 0; i < this.LOD_LEVELS; i++) {
				Generator g = new Generator(i);
	
				g.run2();
	
				System.out.println("lod " + i + "finish");
				// threadPool.execute(g);
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		/*
		 * threadPool.shutdown(); while(threadPool.isTerminated()){
		 * Thread.yield(); } System.out.println("lod terminated");
		 */
	}

	class Generator {
		int lod;
		File objFile, mlxFile, tarFile;
		int tarFaceNum;

		int maxFaceIndex;

		List<Float> vertices;
	//	List<Float> normals;
		List<Integer> faces;

		List<MultiMesh> meshes;

		public Generator(int lod) {
			this.lod = lod;

			String lstFileName;
			if (lod == 0)
				lstFileName = "m.obj";
			else
				lstFileName = (lod-1) + ".obj";
			objFile = new File(srcRoot, lstFileName);
			mlxFile = new File(srcRoot, lod + ".mlx");
			tarFile = new File(srcRoot, lod + ".obj");
			tarFaceNum = (int) (srcFaceNum / Math.pow(1.6, lod));
		}

		public void run2() {
			try {
				generateNewMesh(objFile, mlxFile, tarFile, tarFaceNum);

				System.out.println("generate mesh finish");

				readNewMesh(tarFile);

				System.out.println("read mesh finish");

				divideMesh();

				System.out.println("divide mesh finish");

				saveMesh();

				System.out.println("save mesh finish");

			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		private void saveMesh() throws IOException, InterruptedException {
			SaveDataInBinFile sdibf = new SaveDataInBinFile();

			sdibf.saveMesh(meshes, savingRoot.getAbsolutePath(), lod, null);
		}

		private void divideMesh() {
			long st = System.currentTimeMillis();

			MultiMesh2[] arrayMesh = new MultiMesh2[fileNum];

			int mapSize = maxFaceIndex + 1;

			for (int i = 0; i < fileNum; i++)
				arrayMesh[i] = new MultiMesh2(combinedMeshes.get(i));

			/*
			 * meshes = new ArrayList<MultiMesh2>(); for (int i = 0; i <
			 * fileNum; i++) meshes.add(new MultiMesh2(combinedMeshes.get(i)));
			 */

			int[] indices = new int[3];

			for (int i = 0, l = faces.size(); i < l; i += 3) {// 枚举每个面

				Map<PointInfo, Integer> appearCnt = new HashMap<PointInfo, Integer>();

				for (int j = 0; j < 3; j++) {
					int index = faces.get(i + j);

					indices[j] = index;

					float vx = vertices.get(index * 3);
					float vy = vertices.get(index * 3 + 1);
					float vz = vertices.get(index * 3 + 2);

					Point p = new Point(vx, vy, vz);

					Set<PointInfo> belongsWho = pointsInfo.get(p);// 这个点属于的
																	// [文件和group]

					if (belongsWho == null)
						continue;

					for (PointInfo pi : belongsWho) {
						if (appearCnt.containsKey(pi)) {
							int newCnt = appearCnt.get(pi) + 1;
							appearCnt.put(pi, newCnt);
						} else {
							appearCnt.put(pi, 1);
						}
					}
				}

				PointInfo thisPI = null;
				for (PointInfo pi : appearCnt.keySet()) {
					if (appearCnt.get(pi) == 3) {
						thisPI = pi;
					}
				}
				if (thisPI == null)
					continue;

				arrayMesh[thisPI.fileId].addFace(indices, thisPI.groupId);
			}

			meshes = new ArrayList<MultiMesh>();
			for (int i = 0; i < fileNum; i++) {
				arrayMesh[i].submit(vertices);

				meshes.add(arrayMesh[i]);
			}

			System.out.println("divide time   " + (System.currentTimeMillis() - st));
		}

		private void readNewMesh(File tarFile) throws IOException {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(tarFile)));

			maxFaceIndex = 0;

			vertices = new ArrayList<Float>(tarFaceNum * 9);
		//	normals = new ArrayList<Float>(tarFaceNum * 9);
			faces = new ArrayList<Integer>(tarFaceNum * 3);

			long st = System.currentTimeMillis();

			String line = null;
			while ((line = br.readLine()) != null) {
				String[] params = line.split(" ");
				if (params[0].equals("v")) {
					vertices.add(Float.parseFloat(params[1]));
					vertices.add(Float.parseFloat(params[2]));
					vertices.add(Float.parseFloat(params[3]));

				} else if (params[0].equals("vn")) {
				//	normals.add(Float.parseFloat(params[1]));
				//	normals.add(Float.parseFloat(params[2]));
				//	normals.add(Float.parseFloat(params[3]));

				} else if (params[0].equals("f")) {
					for (int j = 1; j <= 3; j++) {
						int index = Integer.parseInt(params[j]) - 1;
						faces.add(index);
						if (index > maxFaceIndex)
							maxFaceIndex = index;
					}
				}
			}

			System.out.println("read2 time   " + (System.currentTimeMillis() - st));

			br.close();
		}

		final String mlxContent1 = "<FilterScript>"
				+ "<filter name=\"Simplification: Quadric Edge Collapse Decimation\">"
				+ "<Param type=\"RichInt\" value=\"";
		final String mlxContent2 = "\" name=\"TargetFaceNum\"/>"
				+ "<Param type=\"RichFloat\" value=\"0\" name=\"TargetPerc\"/>"
				+ "<Param type=\"RichFloat\" value=\"0.3\" name=\"QualityThr\"/>"
				+ "<Param type=\"RichBool\" value=\"false\" name=\"PreserveBoundary\"/>"
				+ "<Param type=\"RichFloat\" value=\"1\" name=\"BoundaryWeight\"/>"
				+ "<Param type=\"RichBool\" value=\"false\" name=\"PreserveNormal\"/>"
				+ "<Param type=\"RichBool\" value=\"false\" name=\"PreserveTopology\"/>"
				+ "<Param type=\"RichBool\" value=\"false\" name=\"OptimalPlacement\"/>"
				+ "<Param type=\"RichBool\" value=\"false\" name=\"PlanarQuadric\"/>"
				+ "<Param type=\"RichBool\" value=\"false\" name=\"QualityWeight\"/>"
				+ "<Param type=\"RichBool\" value=\"true\" name=\"AutoClean\"/>"
				+ "<Param type=\"RichBool\" value=\"false\" name=\"Selected\"/>" + "</filter>" + "</FilterScript>";

		private void generateNewMesh(File objFile, File mlxFile, File tarFile, int faceNum)
				throws IOException, InterruptedException {
			saveMlxFile(mlxFile, faceNum);
			remeshObj(objFile, mlxFile, tarFile);
		}

		private void saveMlxFile(File mlxFile, int faceNum) throws IOException {
			if (mlxFile.exists())
				mlxFile.delete();

			StringBuffer stringBuffer = new StringBuffer();
			stringBuffer.append(mlxContent1);
			stringBuffer.append(String.valueOf(faceNum));
			stringBuffer.append(mlxContent2);

			String mlx = stringBuffer.toString();

			OutputStream os = new FileOutputStream(mlxFile);
			os.write(mlx.getBytes());
			os.flush();
			os.close();
		}

		private boolean conversionFinish;

		private void remeshObj(File srcObjFile, File mlxFile, File tarObjFile)
				throws IOException, InterruptedException {
			Runtime r = Runtime.getRuntime();
			Process process;

			StringBuffer stringBuffer = new StringBuffer();

			stringBuffer.append("cmd /c meshlabserver -i ");
			stringBuffer.append(srcObjFile.getAbsolutePath());
			stringBuffer.append(" -s ");
			stringBuffer.append(mlxFile.getAbsolutePath());
			stringBuffer.append(" -o ");
			stringBuffer.append(tarObjFile.getAbsolutePath());
		//	stringBuffer.append(" -m vn");

			String cmd = stringBuffer.toString();

			process = r.exec(cmd);
			InputStream processInput = process.getInputStream();
			InputStream processErr = process.getErrorStream();
			conversionFinish = false;
			new Thread() {
				public void run() {
					byte[] b = new byte[100 * 1024];
					while (true) {
						synchronized (LOD.this) {
							if (conversionFinish)
								break;// 转换完成
						}
						try {
							if (processInput.available() > 0) {
								processInput.read(b);// 读取
							}
							if (processErr.available() > 0) {
								processErr.read(b);
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
						Thread.yield();
					}
					try {
						processInput.close();
						processErr.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// System.out.println("process线程结束");
				}
			}.start();

			process.waitFor();
			synchronized (this) {// 关闭子线程
				conversionFinish = true;
			}
		}
	}

}

class PointInfo {
	int fileId, groupId;

	String hashStr;

	public PointInfo(int fileId, int groupId) {
		this.fileId = fileId;
		this.groupId = groupId;
		this.hashStr = String.valueOf(this.fileId) + " " + String.valueOf(this.groupId);
	}

	@Override
	public boolean equals(Object obj) {
		return this.hashStr.equals(((PointInfo) obj).hashStr);
	}

	@Override
	public int hashCode() {// 设fileId不超过300 groupId不超过100000
		return hashStr.hashCode();
	}

}

class Point {
	String pos;

	public Point(float vx, float vy, float vz) {
		pos = String.format("%.4f", vx) + String.format("%.4f", vy) + String.format("%.4f", vz);
	}

	@Override
	public boolean equals(Object obj) {
		return pos.equals(((Point) obj).pos);
	}

	@Override
	public int hashCode() {
		return pos.hashCode();
	}

}
