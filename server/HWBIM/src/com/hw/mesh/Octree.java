package com.hw.mesh;

import java.util.LinkedList;
import java.util.List;

public class Octree {// 八叉树
	BoundingBox boundingBox;// 包围盒
	List<MultiMesh> data;
	boolean isLeaf;
	Octree[] subTrees;

	static final int MIN_GROW_D_AMOUNT = 10;// 少于10个则不再细分
	static final int MAX_DEPTH = 10;

	public Octree(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) {
		boundingBox = new BoundingBox();
		boundingBox.newVertex(minX, minY, minZ);
		boundingBox.newVertex(maxX, maxY, maxZ);
		init();
	}

	public Octree(BoundingBox bb) {
		boundingBox = bb;
		init();
	}

	private void init() {
		isLeaf = true;
		data = new LinkedList<MultiMesh>();
	}

	public void addData(MultiMesh m) {
		data.add(m);
	}

	public int getDataAmount() {
		return data.size();
	}

	public static void transform(Octree node) {// 从根开始生成整棵树
		_transform(node, 0);
	}

	private static void _transform(Octree node, int depth) {
		if (node.getDataAmount() < MIN_GROW_D_AMOUNT || depth > MAX_DEPTH)
			return;// 不必再分
		node.grow();
		for (int i = 0; i < 8; i++)
			_transform(node.subTrees[i], depth + 1);
	}

	private void grow() {// 生成子树
		isLeaf = false;
		subTrees = new Octree[8];
		generateSubBoxes();
		distributeData();
	}

	private void generateSubBoxes() {// 生成子树
		float baseX = boundingBox.minX;
		float baseY = boundingBox.minY;
		float baseZ = boundingBox.minZ;

		float deltaX = (boundingBox.maxX - boundingBox.minX) / 2;
		float deltaY = (boundingBox.maxY - boundingBox.minY) / 2;
		float deltaZ = (boundingBox.maxZ - boundingBox.minZ) / 2;

		int index = 0;
		for (int _x = 0; _x < 2; _x++)
			for (int _y = 0; _y < 2; _y++)
				for (int _z = 0; _z < 2; _z++) {
					float dx = baseX + _x * deltaX;
					float dy = baseY + _y * deltaY;
					float dz = baseZ + _z * deltaZ;

					this.subTrees[index++] = new Octree(dx, dy, dz, dx + deltaX, dy + deltaY, dz + deltaZ);
				}
	}

	private void distributeData() {// data下分给子树
		for (int i = data.size() - 1; i >= 0; i--) {
			for (int j = 0; j < 8; j++) {// 这种方法性能低，但是写起来方便
				// 子树包围盒 包含 第i个数据的包围盒
				// 下发给子树
				if (this.subTrees[j].boundingBox.contains(data.get(i).boundingBox)) {
					this.subTrees[j].addData(data.get(i));
					this.data.remove(i);// 倒着for所以没关系
					break;
				}
			}
		}
	}
}