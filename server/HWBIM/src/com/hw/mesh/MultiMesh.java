package com.hw.mesh;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MultiMesh {
	BoundingBox boundingBox;

	List<Float> vertices;
//	List<Float> vNormals;

	List<MeshGroup> groups;

	public MultiMesh() {
		this.vertices = new ArrayList<Float>();
	//	this.vNormals = new ArrayList<Float>();
		this.groups = new ArrayList<MeshGroup>();
		this.boundingBox = new BoundingBox();
	}

	public void putV(String[] vs) {
		float x, y, z;
		x = Float.parseFloat(vs[0]);
		y = Float.parseFloat(vs[1]);
		z = Float.parseFloat(vs[2]);
		this.vertices.add(x);
		this.vertices.add(y);
		this.vertices.add(z);
		this.boundingBox.newVertex(x, y, z);
	}

/*	public void putVN(String[] vns) {
		for (int i = 0; i < 3; i++) {
			this.vNormals.add(Float.parseFloat(vns[i]));
		}
	}*/

	public void putMeshGroup(MeshGroup mg) {
		this.groups.add(mg);
	}

	public int getVertNum() {
		return vertices.size() / 3;
	}

	public void combMesh(MultiMesh mm) {
		int vOff = this.getVertNum();
		this.vertices.addAll(mm.vertices);
	//	this.vNormals.addAll(mm.vNormals);
		
		
		boolean test=false;
		if(mm.getIfcId()==3)
			test=true;
			
		List<MeshGroup> mGroups = mm.groups;
		for (int i = 0, l = mGroups.size(); i < l; i++) {
			MeshGroup mg = mGroups.get(i);

			MeshGroup tar = new MeshGroup(mg.materialId, mg.ifcId);
			for (int j = 0, lf = mg.faces.size(); j < lf; j++) {
				tar.faces.add(mg.faces.get(j) + vOff);
				
			}
			groups.add(tar);
		}
	}

	public int getIfcId() {// 供g的模型使用
		return groups.get(0).ifcId;
	}

	public int getFaceNum() {
		int fNum = 0;
		for (int i = 0, l = this.groups.size(); i < l; i++) {
			fNum += this.groups.get(i).faces.size();
		}
		return fNum;
	}
}