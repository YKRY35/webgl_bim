package com.hw.mesh;

import java.util.List;

public class BoundingSphere {

	private float minX, minY, minZ;
	private float maxX, maxY, maxZ;
	static final float INF = 200000000;

	public float centerX, centerY, centerZ;
	public float radius;

	public BoundingSphere() {
		minX = minY = minZ = INF;
		maxX = maxY = maxZ = -INF;
	}

	public void newVertex(float x, float y, float z) {
		minX = Math.min(minX, x);
		minY = Math.min(minY, y);
		minZ = Math.min(minZ, z);

		maxX = Math.max(maxX, x);
		maxY = Math.max(maxY, y);
		maxZ = Math.max(maxZ, z);
	}

	public void calc(MultiMesh m) {
		centerX = (minX + maxX) / 2;
		centerY = (minY + maxY) / 2;
		centerZ = (minZ + maxZ) / 2;

		List<Float> vertices = m.vertices;
		double mxR2 = -INF;
		for (int i = 0; i < vertices.size(); i += 3) {
			float x = vertices.get(i);
			float y = vertices.get(i + 1);
			float z = vertices.get(i + 2);

			double r2 = Math.pow(centerX - x, 2) + Math.pow(centerY - y, 2) + Math.pow(centerZ - z, 2);
			if (r2 > mxR2)
				mxR2 = r2;
		}
		radius = (float) Math.sqrt(mxR2);
	}
}