package com.hw.mesh;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hw.utils.LEDataOutputStream;

public class SaveDataInBinFile {

	private LEDataOutputStream ledos;

	/*
	 * 二进制mtl
	 * 
	 * header : material amount 接下来amount 个数据 color specular shininess opacity
	 * int int int float
	 */
	private final String MTL_FILE_NAME = "m.hwbin";

	public void saveMtl(List<MtlContent> mtls, String fp) throws IOException {// fp必须存在
		OutputStream os = new FileOutputStream(new File(fp, MTL_FILE_NAME).getAbsolutePath());

		ledos = new LEDataOutputStream(os);

		int amount = mtls.size();
		ledos.writeIntLE(amount);

		for (int i = 0; i < amount; i++) {
			MtlContent content = mtls.get(i);
			ledos.writeIntLE(content.color);
			ledos.writeIntLE(content.specular);
			ledos.writeIntLE(content.shininess);
			ledos.writeFloatLE(content.opacity);
		}
		ledos.flush();

		ledos.close();
		os.close();
	}

	/*
	 * 
	 * 二进制mesh
	 * 
	 * header : mesh amount,
	 */

	private Map<Integer, MultiMesh> mergeMesh(final List<MultiMesh> meshes, Map<Integer, Integer> ifcId2FileId) {
		Map<Integer, MultiMesh> res = new HashMap<Integer, MultiMesh>();

		if (ifcId2FileId == null) {
			for (int i = 0; i < meshes.size(); i++) {
				res.put(i, meshes.get(i));
			}
			return res;
		}

		// 可以按ifcId排序

		for (MultiMesh mesh : meshes) {
			int ifcId = mesh.getIfcId();
			if (!ifcId2FileId.containsKey(ifcId))// consider IFCSITE
				continue;
			int fileId = ifcId2FileId.get(ifcId);
			if (!res.containsKey(fileId)) {
				res.put(fileId, new MultiMesh());
			}
			res.get(fileId).combMesh(mesh);
		}
		return res;
	}

	final String meshFileName = "m";
	final String meshSuffixName = ".hwbin";

	public Map<Integer, MultiMesh> saveMesh(final List<MultiMesh> meshes, String fp, int lod,
			Map<Integer, Integer> ifcId2FileId) throws IOException, InterruptedException {

		Map<Integer, MultiMesh> tarMeshes = mergeMesh(meshes, ifcId2FileId);

		if (ifcId2FileId == null) {
			for (int fileId : tarMeshes.keySet()) {
	
				String absPath = new File(fp, meshFileName + fileId + "l" + String.valueOf(lod) + meshSuffixName)
						.getAbsolutePath();
				OutputStream os = new FileOutputStream(absPath);
				LEDataOutputStream ledos = new LEDataOutputStream(os);
	
				_saveMesh(tarMeshes.get(fileId), ledos);
	
				ledos.flush();
				os.close();
			}
		}

		return tarMeshes;
	}

	private void _saveMesh(MultiMesh content, LEDataOutputStream ledos) throws IOException {

		boolean useShort = false, addOneShort = false;

		int vAmount = content.vertices.size();
		// int vnAmount = content.vNormals.size();
		int fAmount = content.getFaceNum();
		int gAmount = content.groups.size();

		if (vAmount < 65536)
			useShort = true;
		if (useShort && fAmount % 2 == 1) {
			addOneShort = true;
			fAmount++;
		}

		ledos.writeIntLE(vAmount);
		// ledos.writeIntLE(vnAmount);
		ledos.writeIntLE(fAmount);
		ledos.writeIntLE(gAmount);

		for (int j = 0; j < vAmount; j++) {
			ledos.writeFloatLE(content.vertices.get(j));
		}
		/*
		 * for (int j = 0; j < vnAmount; j++) {
		 * ledos.writeFloatLE(content.vNormals.get(j)); }
		 */
		
		ledos.setUseShort(useShort);

		for (int j = 0; j < gAmount; j++) {
			MeshGroup mg = content.groups.get(j);
			for (int f : mg.faces)
				ledos.writeIntLE(f);
		}
		if (addOneShort)
			ledos.writeIntLE(0);
		
		ledos.setUseShort(false);

		for (int j = 0; j < gAmount; j++) {
			ledos.writeIntLE(content.groups.get(j).ifcId);
			ledos.writeIntLE(content.groups.get(j).faces.size());
			ledos.writeIntLE(content.groups.get(j).materialId);
		}

	}

	/*
	 * 节点数n 接下来n组 数k表示子树首id，k到k+7 数d，数据个数，接下来d个整数，表示meshId 6个float
	 * minX，minY，minZ，maxX，maxY，maxZ表示包围盒
	 */
	final String OCTREE_FILE_NAME = "t.hwbin";

	public void saveOctree(Octree root, String fp) throws IOException {
		OutputStream os = new FileOutputStream(new File(fp, OCTREE_FILE_NAME).getAbsolutePath());

		ledos = new LEDataOutputStream(os);

		Map<Octree, Integer> octreeId = new HashMap<Octree, Integer>();// 树节点到id的映射
		List<Octree> trees = new ArrayList<Octree>();
		trees.add(root);
		int ptr = 0;// 队列
		while (ptr < trees.size()) {// 广度
			Octree t = trees.get(ptr);// 当前id是ptr
			octreeId.put(t, ptr);
			ptr++;
			if (!t.isLeaf) {// 把子树丢入队列
				for (int i = 0; i < 8; i++)
					trees.add(t.subTrees[i]);
			}
		}

		int nodeAmount = trees.size();// 节点总数

		ledos.writeIntLE(nodeAmount);
		for (int i = 0; i < nodeAmount; i++) {
			Octree t = trees.get(i);
			int st = 0;
			if (!t.isLeaf)// 非叶子
				st = octreeId.get(t.subTrees[0]);
			ledos.writeIntLE(st);// 子树首id

			int dataAmount = t.data.size();
			ledos.writeIntLE(dataAmount);// 节点数据个数
			for (int j = 0; j < dataAmount; j++) {
				ledos.writeIntLE(t.data.get(j).getIfcId());
			}

			// 包围盒
			ledos.writeFloatLE(t.boundingBox.minX);
			ledos.writeFloatLE(t.boundingBox.minY);
			ledos.writeFloatLE(t.boundingBox.minZ);
			ledos.writeFloatLE(t.boundingBox.maxX);
			ledos.writeFloatLE(t.boundingBox.maxY);
			ledos.writeFloatLE(t.boundingBox.maxZ);
		}
		ledos.flush();

		ledos.close();
		os.close();
	}

}