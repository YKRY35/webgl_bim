package com.hw.mesh;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MultiMesh2 extends MultiMesh {

	List<Integer>[] srcFaces;
	int groupNum;
	int mnId, mxId;

	public MultiMesh2(final MultiMesh src) {// 减面前的模型
		super();
		init(src);
	}

	private void init(final MultiMesh src) {
		for (MeshGroup mg : src.groups) {
			super.groups.add(new MeshGroup(mg.materialId, mg.ifcId));
		}
		groupNum = src.groups.size();
		srcFaces = new List[groupNum];
		for (int i = 0; i < groupNum; i++)
			srcFaces[i] = new ArrayList<Integer>();

		mnId = 1000000000;
		mxId = -1;
	}

	void addFace(int[] indices, int groupId) {
		for (int i = 0; i < 3; i++) {
			srcFaces[groupId].add(indices[i]);
			if (indices[i] > mxId)
				mxId = indices[i];
			if (indices[i] < mnId)
				mnId = indices[i];
		}
	}

	void submit(List<Float> vert) {
		mxId++;
		int range = mxId - mnId;
		if (range <= 0)
			return;
		int[] idxMap = new int[range];
		for (int i = 0; i < range; i++)
			idxMap[i] = -1;

		int curMappedIndex = 0;
		for (int g = 0; g < groupNum; g++) {
			List<Integer> f = srcFaces[g];
			for (int srcIndex : f) {
				int convId = srcIndex - mnId;
				int mappedId = idxMap[convId];
				if (mappedId == -1) {// 不存在，新建映射
					for (int j = 0; j < 3; j++) {
						super.vertices.add(vert.get(srcIndex * 3 + j));
					//	super.vNormals.add(norm.get(srcIndex * 3 + j));
					}
					mappedId = curMappedIndex++;
					idxMap[convId] = mappedId;
				}
				super.groups.get(g).faces.add(mappedId);
			}
		}

	}

}