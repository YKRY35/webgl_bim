package com.hw.mesh;

public class BoundingBox {// ��Χ��
	float minX, minY, minZ;
	float maxX, maxY, maxZ;
	static final float INF = 200000000;

	public BoundingBox() {
		minX = minY = minZ = INF;
		maxX = maxY = maxZ = -INF;
	}

	public void newVertex(float x, float y, float z) {
		minX = Math.min(minX, x);
		minY = Math.min(minY, y);
		minZ = Math.min(minZ, z);

		maxX = Math.max(maxX, x);
		maxY = Math.max(maxY, y);
		maxZ = Math.max(maxZ, z);
	}

	public boolean contains(BoundingBox b) {// this �Ƿ���� b
		if (b.minX < minX)
			return false;
		if (b.maxX > maxX)
			return false;
		if (b.minY < minY)
			return false;
		if (b.maxY > maxY)
			return false;
		if (b.minZ < minZ)
			return false;
		if (b.maxZ > maxZ)
			return false;
		return true;
	}

	public String toString() {
		return "minX maxX minY maxY minZ maxZ " + minX + " " + maxX + " " + minY + " " + maxY + " " + minZ + " " + maxZ;
	}
}

