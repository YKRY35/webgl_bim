package com.hw.outside;

public class FileInfo {
	String srcFileName;// 源文件名称
	String alias;// 别名
	String state;// 当前状态

	public FileInfo(String alias, String srcFileName, String state) {
		this.srcFileName = srcFileName;
		this.alias = alias;
		this.state = state;
	}

	public String getSrcFileName() {
		return srcFileName;
	}

	public String getAlias() {
		return alias;
	}

	public String getState() {
		return state;
	}
}