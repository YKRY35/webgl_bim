package com.hw.outside;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import com.hw.decoder.IfcDecoder;
import com.hw.decoder.ThreeDDecoder;
import com.hw.mesh.LOD;
import com.hw.mesh.MultiMesh;
import com.hw.mesh.SaveDataInBinFile;
import com.hw.utils.DBUtil;

public class ConversionTask implements Runnable {
	private static final Object lock = new Object();
	private static final Object revitLock = new Object();

	private static final String srcFileFolder1 = "F:/PROJECTS/PROGRAMMING/Web/nginx_bim/data/resources/HWBIM/";
	private static final String MESH_FOLDER = "mesh";
	private static final String MATERIAL_FOLDER = "material";
	private static final String OCTREE_FOLDER = "octree";
	private static final String TMP_FOLDER = "tmp";
	private static final String TMP_FILE_NAME_OBJ = "m.obj";
	private static final String TMP_FILE_NAME_MTL = "m.mtl";

	private final String tarFileFolder1;

	private String srcFilePath;
	private ConversionCallback callback;

	private boolean conversionFinish = false;

	private String alias;// 当前模型的别名

	public ConversionTask(String alias, ConversionCallback callback) {
		this.tarFileFolder1 = srcFileFolder1 + alias + "/decoded";
		new File(this.tarFileFolder1).mkdirs();// 文件夹不存在创建
	
		this.srcFilePath = srcFileFolder1 + alias + "/SrcFile/" + DBUtil.aliasToSrcName(alias);
		this.callback = callback;
		this.alias = alias;

	}

	@Override
	public void run() {// 模型转换
		try {
			String fileName=DBUtil.aliasToSrcName(alias);//文件名
			
			int pPos=fileName.lastIndexOf('.');
			
			String name=fileName.substring(0, pPos);
			String suffix=fileName.substring(pPos+1, fileName.length());
			
			
			if(suffix.equals("rvt")||suffix.equals("rfa")||suffix.equals("adsk")||suffix.equals("rte")){//revit文件，转换为ifc
				synchronized(revitLock){
					DBUtil.setRCFilePath(this.srcFilePath);
					DBUtil.setRevitServerState(1);
					
					while(true){
						if(DBUtil.getRevitServerState()==3){//转换完成
							break;
						}
						Thread.sleep(700);
					}
					
					DBUtil.setRevitServerState(0);
				}
				this.srcFilePath = srcFileFolder1 + alias + "/SrcFile/"+name+"."+"ifc";
			}
			
			decodeIfcFile();
			

			DBUtil.setAliasState(alias, DBUtil.ALIAS_CONVERTED);// 设置转换完成


			if (callback != null)
				callback.onTaskFinish();// 完成，回调接口

		} catch (Exception e) {// 接收一切异常
			DBUtil.setAliasState(alias, DBUtil.ALIAS_NOT_CONVERTED);// 失败，设置为未转换
			e.printStackTrace();
		}

	}

	private void decodeIfcFile() throws IOException, InterruptedException{
		long startTime = System.currentTimeMillis();

		File meshRoot = new File(this.tarFileFolder1, MESH_FOLDER);
		File materialRoot = new File(this.tarFileFolder1, MATERIAL_FOLDER);
		File octreeRoot = new File(this.tarFileFolder1, OCTREE_FOLDER);
		File tmpFileRoot = new File(this.tarFileFolder1, TMP_FOLDER);

		meshRoot.mkdir();
		materialRoot.mkdir();
		octreeRoot.mkdir();
		tmpFileRoot.mkdir();// 创建文件夹

		File tmpObjFile = new File(tmpFileRoot, TMP_FILE_NAME_OBJ);
		File tmpMtlFile = new File(tmpFileRoot, TMP_FILE_NAME_MTL);

		IfcDecoder ifcDecoder = new IfcDecoder();
		SaveDataInBinFile sdibf = new SaveDataInBinFile();
		ThreeDDecoder tdDecoder = new ThreeDDecoder();

		Runtime runtime = Runtime.getRuntime();

		final String _cmd = "cmd /c IfcConvert64 " + srcFilePath + " " + tmpObjFile.getAbsolutePath();
		Process process = runtime.exec(_cmd);// 几何信息提取

		InputStream processInput = process.getInputStream();
		InputStream processErr = process.getErrorStream();
		conversionFinish = false;
		new Thread() {
			public void run() {
				byte[] b = new byte[100 * 1024];
				while (true) {
					synchronized (ConversionTask.this) {
						if (conversionFinish)
							break;// 转换完成
					}
					try {
						if (processInput.available() > 0) {
							processInput.read(b);// 读取
						}
						if (processErr.available() > 0) {
							processErr.read(b);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					Thread.yield();
				}
				try {
					processInput.close();
					processErr.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("process线程结束");
			}
		}.start();

		process.waitFor();
		synchronized (this) {// 关闭子线程
			conversionFinish = true;
		}

		System.out.println("ifcConvert64 finish   " + (System.currentTimeMillis() - startTime));

		tdDecoder.decode(tmpObjFile.getAbsolutePath(), tmpMtlFile.getAbsolutePath());

		ifcDecoder.readAndDecode(lock, this.srcFilePath, tdDecoder.getMeshAmount(), tdDecoder.get_mapIfcName2Id());

		System.out.println("ifcDecoder finish   " + (System.currentTimeMillis() - startTime));

		sdibf.saveMtl(tdDecoder.getMtls(), materialRoot.getAbsolutePath());
		Map<Integer, MultiMesh> combinedMeshes=sdibf.saveMesh(tdDecoder.getMeshes(), meshRoot.getAbsolutePath(), 0, ifcDecoder.get_familyTypes());

		
		DBUtil.saveIfcAttrs(this.alias, ifcDecoder.getElemAttr());

		// 构件树写入数据库
		DBUtil.saveConstructTree(alias, ifcDecoder.getConstructTree().toJSON());

		// 设备树写入数据库
		DBUtil.saveEquipmentTree(alias, ifcDecoder.get_quipments().toJSON());

	//	DBUtil.saveTriangleAmount(alias, tdDecoder.triangleNum);// 保存三角形数量

		sdibf.saveOctree(tdDecoder.getOctree(), octreeRoot.getAbsolutePath());

		DBUtil.saveBoundingSpheres(alias, tdDecoder.getBoundingSpheres());
		
		LOD lod=new LOD(combinedMeshes, tmpFileRoot, meshRoot);
		
		runtime.exec("cmd /c gzip -k " + meshRoot.getAbsolutePath() + "\\*.hwbin");
	}
}
