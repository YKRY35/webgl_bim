package com.hw.outside;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConversionThreadPool {
	private static ConversionThreadPool instance;

	public static ConversionThreadPool getInstance() {
		if (instance == null)
			instance = new ConversionThreadPool();
		return instance;
	}

	final static int THREAD_POOL_SIZE = 3;// �̳߳ش�С
	ExecutorService threadPool;

	private Object lock;

	public ConversionThreadPool() {
		threadPool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		lock = new Object();
	}

	public void addTask(ConversionTask task) {
		threadPool.execute(task);
	}
}