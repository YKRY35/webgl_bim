package com.hw.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisUtil {

	private static JedisUtil instance = null;

	public static JedisUtil getInstance() {
		if (instance == null)
			instance = new JedisUtil();
		return instance;
	}

	private JedisPool pool;

	public JedisUtil() {
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(10);
		pool = new JedisPool("localhost");
	}

	private Jedis getJedis() {
		return pool.getResource();
	}

	private void returnResource(final Jedis jedis) {
		if (jedis != null) {
			pool.returnResource(jedis);
		}
	}

	int llen(String key) {
		Jedis jedis = getJedis();
		int ret = jedis.llen(key).intValue();
		returnResource(jedis);
		return ret;
	}

	void rpush(String key, String value) {
		Jedis jedis = getJedis();
		jedis.rpush(key, value);
		returnResource(jedis);
	}

	void set(String key, String value) {
		Jedis jedis = getJedis();
		jedis.set(key, value);
		returnResource(jedis);
	}

	String get(String key) {
		Jedis jedis = getJedis();
		String ret = jedis.get(key);
		returnResource(jedis);
		return ret;
	}

	String lindex(String key, int index) {
		Jedis jedis = getJedis();
		String ret = jedis.lindex(key, index);
		returnResource(jedis);
		return ret;
	}

}