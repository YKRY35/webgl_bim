package com.hw.utils;

import java.util.Random;

import net.sf.json.JSONObject;
import redis.clients.jedis.Jedis;

public class DBUtil {// 数据库操作

	static Random random=new Random(System.currentTimeMillis());


	static final String File_Files = "File:Files";// 文件别名列表
	static final String File_SrcName = "File:SrcName:";// 别名对应源文件名
	static final String File_AliasState = "File:AliasState:";// 文件别名mark

	public static final int ALIAS_NOT_EXIST = 0;// 模型不存在
	public static final int ALIAS_NOT_CONVERTED = 1;// 模型未转换
	public static final int ALIAS_CONVERTING = 2;// 模型转换中
	public static final int ALIAS_CONVERTED = 3;// 模型已转换

	static final String ALIAS_STATE_NOT_CONVERTED = "not_converted";
	static final String ALIAS_STATE_CONVERTING = "converting";
	static final String ALIAS_STATE_CONVERTED = "converted";

	static final int ALIAS_LENGTH = 8;

	public static String newFile(String fileName) {// 用户上传文件
		JedisUtil redis=JedisUtil.getInstance();
		
		String alias = getFileAlias();// 获得文件别名
		redis.rpush(File_Files, alias);// 丢入文件列表
		redis.set(File_SrcName + alias, fileName);// 别名 到 源文件名 的映射
		redis.set(File_AliasState + alias, ALIAS_STATE_NOT_CONVERTED);// 标记这个别名为未转换
		return alias;
	}

	public static String[] getFileList() {
		JedisUtil redis=JedisUtil.getInstance();
		
		int len = redis.llen(File_Files);
		String[] tmp = new String[len];
		for (int i = 0; i < len; i++) {
			tmp[i] = redis.lindex(File_Files, i);
		}
		return tmp;
	}

	public static String aliasToSrcName(String alias) {
		JedisUtil redis=JedisUtil.getInstance();
		return redis.get(File_SrcName + alias);
	}

	public static String getAliasState(String alias) {
		JedisUtil redis=JedisUtil.getInstance();
		return redis.get(File_AliasState + alias);
	}

	public static void setAliasState(String alias, int stateCode) {
		JedisUtil redis=JedisUtil.getInstance();
		/*
		 * 设置状态 上传文件时设置了未转换 这里可以设置转换中，转换完成
		 */
		if (stateCode == ALIAS_CONVERTING)
			redis.set(File_AliasState + alias, ALIAS_STATE_CONVERTING);
		else if (stateCode == ALIAS_CONVERTED) {
			redis.set(File_AliasState + alias, ALIAS_STATE_CONVERTED);
		}
	}

	private static String getFileAlias() {// 生成一个8位文件别名
		String tmp;
		while (true) {
			StringBuffer stringBuffer = new StringBuffer(ALIAS_LENGTH);
			for (int i = 0; i < ALIAS_LENGTH; i++) {
				stringBuffer.append((char) (random.nextInt(26) + 'a'));// a -- z
			}
			tmp = stringBuffer.toString();
			if (getAliasState_code(tmp) == 0)// 数据库中不存在
				break;
		}
		// System.out.println("alias "+tmp);
		return tmp;
	}

	public static int getAliasState_code(String alias) {// 获取这个别名对应模型的状态
		String state = getAliasState(alias);
		if (state == null)
			return ALIAS_NOT_EXIST;
		if (state.equals(ALIAS_STATE_NOT_CONVERTED))
			return ALIAS_NOT_CONVERTED;
		if (state.equals(ALIAS_STATE_CONVERTING))
			return ALIAS_CONVERTING;
		return ALIAS_CONVERTED;
	}

	// ===========================================================

	static final String IfcAttr = "IfcAttr:";// ifc属性

	private static String ifcAttrKey(String ifcAlias, String ifcId) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(IfcAttr);
		stringBuffer.append(ifcAlias);
		stringBuffer.append(':');
		stringBuffer.append(String.valueOf(ifcId));
		return stringBuffer.toString();
	}

	public static void saveIfcAttrs(String alias, JSONObject[] objs) {
		for (int i = 0, l = objs.length; i < l; i++) {
			if (objs[i] != null)
				_saveIfcAttr(alias, String.valueOf(i), objs[i].toString());
		}
	}

	private static void _saveIfcAttr(String ifcAlias, String ifcId, String value) {// ifc属性
		JedisUtil redis=JedisUtil.getInstance();
		String key = ifcAttrKey(ifcAlias, ifcId);
		if (value == null)
			value = "{}";
		redis.set(key, value);
	}

	public static String getIfcAttr(String ifcAlias, String ifcId) {
		JedisUtil redis=JedisUtil.getInstance();
		String key = ifcAttrKey(ifcAlias, ifcId);
		String value = redis.get(key);
		if (value == null)
			return "{}";
		return value;
	}

	private static final String MODEL_TRIANGLE_AMOUNT_KEY = "IfcModel:TriangleAmount:";
	public static final String MODEL_CONSTRUCT_TREE_KEY = "IfcModel:ConstructTree:";
	public static final String MODEL_EQUIPMENT_TREE_KEY = "IfcModel:EquipmentTree:";
	public static final String MODEL_BOUNDING_SPHERES = "IfcModel:BoundingSpheres:";

	public static void saveTriangleAmount(String alias, int tAmount) {
		JedisUtil redis=JedisUtil.getInstance();
		redis.set(MODEL_TRIANGLE_AMOUNT_KEY + alias, String.valueOf(tAmount));
	}

	public static String getTriangleAmount(String alias) {
		JedisUtil redis=JedisUtil.getInstance();
		return redis.get(MODEL_TRIANGLE_AMOUNT_KEY + alias);
	}

	public static void saveConstructTree(String alias, String tree) {
		JedisUtil redis=JedisUtil.getInstance();
		redis.set(MODEL_CONSTRUCT_TREE_KEY + alias, tree);
	}

	public static String getConstructTree(String alias) {
		JedisUtil redis=JedisUtil.getInstance();
		return redis.get(MODEL_CONSTRUCT_TREE_KEY + alias);
	}

	public static void saveEquipmentTree(String alias, String tree) {
		JedisUtil redis=JedisUtil.getInstance();
		redis.set(MODEL_EQUIPMENT_TREE_KEY + alias, tree);
	}

	public static String getEquipmentTree(String alias) {
		JedisUtil redis=JedisUtil.getInstance();
		return redis.get(MODEL_EQUIPMENT_TREE_KEY + alias);
	}
	
	public static void saveBoundingSpheres(String alias, String bss) {
		JedisUtil redis=JedisUtil.getInstance();
		redis.set(MODEL_BOUNDING_SPHERES + alias, bss);
	}

	public static String getBoundingSpheres(String alias) {
		JedisUtil redis=JedisUtil.getInstance();
		return redis.get(MODEL_BOUNDING_SPHERES + alias);
	}

	// revit
	public static final String REVIT_SERVICE_STATE = "Revit:State";
	public static final String REVIT_FILE_PATH = "Revit:ConvertToIfc";

	public static int getRevitServerState() {
		JedisUtil redis=JedisUtil.getInstance();
		String state = redis.get(REVIT_SERVICE_STATE);
		if (state == null)
			return 0;
		return Integer.parseInt(state);
	}

	public static void setRevitServerState(int state) {
		JedisUtil redis=JedisUtil.getInstance();
		redis.set(REVIT_SERVICE_STATE, String.valueOf(state));
	}

	public static void setRCFilePath(String path) {
		JedisUtil redis=JedisUtil.getInstance();
		redis.set(REVIT_FILE_PATH, path);
	}

	// equipment
	public static final String EQUIP_DATA = "Equip:";

	public static String getEquipData(String alias, String id) {
		JedisUtil redis=JedisUtil.getInstance();
		return redis.get(EQUIP_DATA + alias + ":" + id);
	}

	public static void setEquipData(String alias, String id, String data) {
		JedisUtil redis=JedisUtil.getInstance();
		redis.set(EQUIP_DATA + alias + ":" + id, data);
	}

}