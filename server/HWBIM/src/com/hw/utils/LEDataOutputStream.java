package com.hw.utils;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class LEDataOutputStream extends DataOutputStream {

	private ByteBuffer byteBuffer;

	final int WHEN_TO_FLUSH = 9;

	private boolean useShort = false;

	public void setUseShort(boolean useShort) {
		this.useShort = useShort;
	}

	public LEDataOutputStream(OutputStream arg0) {
		super(arg0);
		byteBuffer = ByteBuffer.allocate(30 * 1024);// 30MB
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
	}

	public void writeIntLE(int x) throws IOException {
		if (useShort) {
			byteBuffer.putShort((short) x);
		} else
			byteBuffer.putInt(x);
		if (byteBuffer.remaining() < WHEN_TO_FLUSH)
			flush();
	}

	public void writeFloatLE(float x) throws IOException {
		byteBuffer.putFloat(x);
		if (byteBuffer.remaining() < WHEN_TO_FLUSH)
			flush();
	}

	public void flush() throws IOException {
		super.write(byteBuffer.array(), 0, byteBuffer.position());
		byteBuffer.clear();
	}
}